package pairg.handyhome;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteHelper extends SQLiteOpenHelper{
	// nome da tabela
	public static final String TABLE_IMAGES = "images";
	// colunas
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_IMAGE_NAME = "name";
	public static final String COLUMN_IMAGE = "image";
	// nome e versão do banco de dados
	private static final String DATABASE_NAME = "imagesdb";
	private static final int DATABASE_VERSION = 1;
	
	// criação da declaração do banco de dados
	private static final String DATABASE_CREATE = "create table "
			+ TABLE_IMAGES + "(" + COLUMN_ID
			+ " integer primary key autoincrement, " + COLUMN_IMAGE_NAME
			+ " text not null, " + COLUMN_IMAGE
			+ "blob);";
	
	// construtor
	public MySQLiteHelper(Context context){
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}	
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(MySQLiteHelper.class.getName(),
				"Atualizando banco de dados da versão " + oldVersion + " para "
			            + newVersion + ", isso irá destruir todos os dados antigos.");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_IMAGES);
		onCreate(db);
	}
	
	// implementação da CRUD (CREATE, READ, UPDATE, DELETE)
	
	// adicionar imagem ao banco de dados
	public void addImage(Image image){
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put(COLUMN_IMAGE_NAME, image.get_name());
		values.put(COLUMN_IMAGE, image.get_image());
		
		// inserindo linha na tabela
		db.insert(TABLE_IMAGES, null, values);
		db.close();
	}
	
	// busca por uma unica imagem
	public Image getImage(String name){
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(TABLE_IMAGES, new String[] {COLUMN_ID,
				COLUMN_IMAGE_NAME, COLUMN_IMAGE}, COLUMN_IMAGE_NAME + "=?",
				new String[] {name}, null, null, null);
		if(cursor != null){
			cursor.moveToFirst();
		}

		Image image = new Image(Integer.parseInt(cursor.getString(0)),
				cursor.getString(1), cursor.getBlob(2));
		
		return image;
	}
	
	// pegar todas as imagens
	public List<Image> getAllImages(){
		List<Image> imageList = new ArrayList<Image>();
		
		// consulta
		String selectQuery = "SELECT * FROM " + TABLE_IMAGES + " ORDER BY " + COLUMN_IMAGE_NAME;
		
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		
		// percorre o cursor adicionando as linhas a lista
		if(cursor.moveToFirst()){
			do{
				Image image = new Image();
				image.set_id(Integer.parseInt(cursor.getString(0)));
				image.set_name(cursor.getString(1));
				image.set_image(cursor.getBlob(2));
				
				// adiciona a lista
				imageList.add(image);
			}while(cursor.moveToNext());
		}
		
		db.close();
		
		return imageList;
		
	}
	
	// pegar número de imagens
	public int getImageCount(){
		String countQuerry = "SELECT * FROM " + TABLE_IMAGES;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuerry, null);
		db.close();
		
		return cursor.getCount();
	}
}
