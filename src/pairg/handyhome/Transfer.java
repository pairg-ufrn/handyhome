package pairg.handyhome;

public class Transfer {

	private static int hasAppliance = 0;
	private static Appliance ap;

	public static void setAppliance(Appliance a) {
		hasAppliance = 1;
		ap = a;
	}
	
	public static Appliance getAppliance() {
		return ap;
	}
	
	public static void clearAppliance(){
		hasAppliance = 0;
		ap = null;
	}
}
