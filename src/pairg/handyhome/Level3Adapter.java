package pairg.handyhome;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.util.Log;
import android.util.StateSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.LinearLayout.LayoutParams;
import pairg.handyhome.Oraculo;
public class Level3Adapter extends ArrayAdapter<Service>{
	private final Context context;
	private final ArrayList<Service> services;
	private final Appliance appliance;
	private int r = 255;
	private int g = 255;
	private int b = 255;
	
	public Level3Adapter(Context context, ArrayList<Service> services, Appliance appliance, int pos){
		super(context, R.layout.rowlayout, services);
		this.context = context;
		this.services = services;
		this.appliance = appliance;
		setColor(pos);
	}
	
	private void setColor(int pos){
		int rt = pos % 7;
		switch(rt){
		case 0: //0 green nephritis
			this.r=39; this.g=174; this.b=96;
			break;
		case 1: //1 blue belize hole
			this.r=41; this.g=128; this.b=185;
			break;
		case 2: //2 red alizarin
			this.r=231; this.g=76; this.b=60;
			break;
		case 3: //3 purple amethyst
			this.r=155; this.g=89; this.b=182;
			break;
		case 4: //4 yellow 
			this.r=204; this.g=156; this.b=0;
			break;
		case 5: //5 azul petroleo wet asphalt
			this.r=52; this.g=73; this.b=94;
			break;
		case 6: //6 orange carrot
			this.r=230; this.g=126; this.b=34;
			break;
		}
	}
	

	private StateListDrawable createStateListDrawable() {
	    StateListDrawable stateListDrawable = new StateListDrawable();
	    OvalShape ovalShape = new OvalShape();
	    ShapeDrawable shapeDrawable = new ShapeDrawable(ovalShape);
	    ////shapeDrawable.getPaint().setColor(context.getResources().getColor(R.color.blue));
	    //shapeDrawable.getPaint().setColor(Color.rgb(102,102,102));
	    shapeDrawable.getPaint().setColor(Color.rgb(this.r, this.g, this.b));
	    stateListDrawable.addState(new int[] { android.R.attr.state_pressed }, shapeDrawable);
	    stateListDrawable.addState(StateSet.WILD_CARD, shapeDrawable);
	    return stateListDrawable;
	}
	
	private Bitmap createRoundImage(Bitmap img){
		Bitmap circleBitmap = Bitmap.createBitmap(img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);
	    BitmapShader shader = new BitmapShader(img, TileMode.CLAMP, TileMode.CLAMP);
	    Paint paint = new Paint();
	    paint.setAntiAlias(true);
	    paint.setShader(shader);
	    Canvas c = new Canvas(circleBitmap);
	    c.drawCircle(img.getWidth() / 2, img.getHeight() / 2, img.getWidth() / 2, paint);
	    return circleBitmap;
	}
	
	/*@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		TextView textView = new TextView(context);
		ImageView imageView = new ImageView(context);
		LinearLayout linearLayout = new LinearLayout(context);
		
		linearLayout.addView(imageView);
		linearLayout.addView(textView);
		
		//Aqui se coloca a imagem de cada servi�o
		//imageView.setBackgroundResource(R.drawable.ic_launcher);
		//imageView.setImageBitmap(appliance.getVisualImage());
		Bitmap img = JSONInterpreter.getImageFromName(services.get(position).getServImage());
		imageView.setImageBitmap(img);
		
		//Aqui se coloca o texto de cada servi�o
		textView.setText(services.get(position).getServName());
    	// Faz consulta ao banco de dados para pegar imagem
    	// Aqui o banco de dados será simulado pelo resources devido ao tempo
    	//imageView.setBackground(background);
    	int serviceNumber = Oraculo.getInterface(appliance, position);
    	ArrayList<Integer> sbValues;
    	ArrayList<Integer> dtValues;
    	ArrayList<Integer> hrValues;
    	
    	switch(serviceNumber){
    		case 1:
    			ToggleButton actionToggleButton = new ToggleButton(context);
    			ToggleButtonValues tbValues = Oraculo.getToggleButtonValues(appliance, position);
    			actionToggleButton.setChecked(tbValues.getValue());
    			
    			if(actionToggleButton.isChecked())
    				actionToggleButton.setText(tbValues.getNameTrue());
    			else
    				actionToggleButton.setText(tbValues.getNameFalse());
    			
    			linearLayout.addView(actionToggleButton);
    			break;
    		case 2:
    			SeekBar actionSeekBar = new SeekBar(context);
    			sbValues = Oraculo.getSeekBarOrTwoButtonsAndVisorValues(appliance, position);
    			actionSeekBar.setProgress(sbValues.get(0)+sbValues.get(1));
    			actionSeekBar.setMax(sbValues.get(2));
    			
    			linearLayout.addView(actionSeekBar);
    			break;
    		case 3:
    			Button btnUp = new Button(context);
    			Button btnDown = new Button(context);
    			TextView txtVisor = new TextView(context);
    			sbValues = Oraculo.getSeekBarOrTwoButtonsAndVisorValues(appliance, position);
    			
    			btnUp.setText("+");
    			btnDown.setText("-");
    			txtVisor.setText(sbValues.get(0).toString());
    			linearLayout.addView(btnUp);
    			linearLayout.addView(btnDown);
    			linearLayout.addView(txtVisor);
    			break;
    		case 4:
    			Button btn = new Button(context);
    			linearLayout.addView(btn);
    			break;
    		case 5:
    			Spinner actionSpinner = new Spinner(context);
    			ArrayList<String> spValues = Oraculo.getSpinnerValues(appliance, position);
    			ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, spValues);
    			ArrayAdapter<String> spinnerArrayAdapter = arrayAdapter;
    			spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
    			actionSpinner.setAdapter(spinnerArrayAdapter);
    			linearLayout.addView(actionSpinner);
    			break;
    		case 6:
    			Button btnWithString = new Button(context);
    			linearLayout.addView(btnWithString);
    			break;
    		case 7:
    			Button btnWithStatus = new Button(context);
    			linearLayout.addView(btnWithStatus);
    			break;
    		case 8:
    			Button btnDate = new Button(context);
    			TextView txtDate = new TextView(context);
    			dtValues = Oraculo.getDateValues(appliance, position);
    			txtDate.setText("(8)Date="+dtValues.get(0)+"/"+dtValues.get(1)+"/"+dtValues.get(2));
    			linearLayout.addView(btnDate);
    			linearLayout.addView(txtDate);
    			break;
    		case 9:
    			Button btnHour = new Button(context);
    			TextView txtHour = new TextView(context);
    			hrValues = Oraculo.getHourValues(appliance, position);
    			txtHour.setText(hrValues.get(0)+":"+hrValues.get(1));
    			linearLayout.addView(btnHour);
    			linearLayout.addView(txtHour);
    			break;
    		case 10:
    			Button btnResponse = new Button(context);
    			linearLayout.addView(btnResponse);
    			break;
    		case 11:
    			Button btnFile = new Button(context);
    			linearLayout.addView(btnFile);
    			break;
    		case 12:
    			Button btnReturn = new Button(context);
    			linearLayout.addView(btnReturn);
    			break;
    	}
		
		
		return linearLayout;
	}*/
	
	@SuppressLint("NewApi")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View row = convertView;
		RecordHolder holder = null;


			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			//(A) To use the layout with vertical line, inflate with: R.layout.rowlayout_withverticalline
			//and uses PARTL3AVerticalLine code
			//(B) To use the layout with no line, inflate with: R.layout.rowlayout
			//and comment PARTL3AVericalLine code
			row = inflater.inflate(R.layout.rowlayout_withverticalline, parent, false);

			holder = new RecordHolder();
			holder.serviceImage = (ImageView) row.findViewById(R.id.service_image);
			holder.serviceName = (TextView) row.findViewById(R.id.service_name);
			holder.serviceLayoutControl = (LinearLayout) row.findViewById(R.id.linearlayoutcontrol);
			row.setTag(holder);
			
		//PARTL3AVerticalLine - This part of the code is part of the vertical line layout
		//Attention: This code is used if R.layout.rowlayout_withverticalline is inflated
		//It checks if the item is the last row (the last row must show only part of the vertical line, until the half of row height.
		//If it is the last row, changes the height of the View which represents the vertical line.
		//The new height is given in px, but is converted from desired size in dp (in variable "dpsize").
		if(position==(getCount()-1)){
			View my_line = (View) row.findViewById(R.id.lineconnectingservices);
			int dpsize = 40; //40dp
			int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpsize, context.getResources().getDisplayMetrics());
			my_line.getLayoutParams().height = px;
		}
		//END OF PARTL3AVerticalLine
		
		//alterando a cor da linha vertical
		View verticalline = (View) row.findViewById(R.id.lineconnectingservices);
		verticalline.setBackgroundColor(Color.rgb( this.r,this.g,this.b ));
		
		Bitmap img = JSONInterpreter.getImageFromName(services.get(position).getServImage());
		//holder.serviceImage.setBackgroundColor(context.getResources().getColor(R.color.blue));
		//holder.serviceImage.setImageBitmap(img);
		holder.serviceImage.setBackground(createStateListDrawable());
		holder.serviceImage.setImageBitmap(createRoundImage(img));
		holder.serviceName.setText(services.get(position).getServName());
		
		int serviceNumber = Oraculo.getInterface(appliance, position);
    	ArrayList<Integer> sbValues;
    	ArrayList<Integer> dtValues;
    	ArrayList<Integer> hrValues;
    	
    	switch(serviceNumber){
    		case 1:
    			ToggleButton actionToggleButton = new ToggleButton(context);
    			ToggleButtonValues tbValues = Oraculo.getToggleButtonValues(appliance, position);
    			actionToggleButton.setChecked(tbValues.getValue());
    			
    			if(actionToggleButton.isChecked())
    				actionToggleButton.setText(tbValues.getNameTrue());
    			else
    				actionToggleButton.setText(tbValues.getNameFalse());
    			
    			holder.serviceLayoutControl.addView(actionToggleButton);
    			break;
    		case 2:
    			SeekBar actionSeekBar = new SeekBar(context);
    			sbValues = Oraculo.getSeekBarOrTwoButtonsAndVisorValues(appliance, position);
    			actionSeekBar.setProgress(sbValues.get(0)+sbValues.get(1));
    			actionSeekBar.setMax(sbValues.get(2));
    			
    			holder.serviceLayoutControl.addView(actionSeekBar);
    			break;
    		case 3:
    			Button btnUp = new Button(context);
    			Button btnDown = new Button(context);
    			TextView txtVisor = new TextView(context);
    			sbValues = Oraculo.getSeekBarOrTwoButtonsAndVisorValues(appliance, position);
    			
    			btnUp.setText("+");
    			btnDown.setText("-");
    			txtVisor.setText(sbValues.get(0).toString());
    			holder.serviceLayoutControl.addView(btnUp);
    			holder.serviceLayoutControl.addView(btnDown);
    			holder.serviceLayoutControl.addView(txtVisor);
    			break;
    		case 4:
    			Button btn = new Button(context);
    			holder.serviceLayoutControl.addView(btn);
    			break;
    		case 5:
    			Spinner actionSpinner = new Spinner(context);
    			ArrayList<String> spValues = Oraculo.getSpinnerValues(appliance, position);
    			ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, spValues);
    			ArrayAdapter<String> spinnerArrayAdapter = arrayAdapter;
    			spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
    			actionSpinner.setAdapter(spinnerArrayAdapter);
    			holder.serviceLayoutControl.addView(actionSpinner);
    			break;
    		case 6:
    			Button btnWithString = new Button(context);
    			holder.serviceLayoutControl.addView(btnWithString);
    			break;
    		case 7:
    			Button btnWithStatus = new Button(context);
    			holder.serviceLayoutControl.addView(btnWithStatus);
    			break;
    		case 8:
    			Button btnDate = new Button(context);
    			TextView txtDate = new TextView(context);
    			dtValues = Oraculo.getDateValues(appliance, position);
    			txtDate.setText("Date="+dtValues.get(0)+"/"+dtValues.get(1)+"/"+dtValues.get(2));
    			holder.serviceLayoutControl.addView(btnDate);
    			holder.serviceLayoutControl.addView(txtDate);
    			break;
    		case 9:
    			Button btnHour = new Button(context);
    			TextView txtHour = new TextView(context);
    			hrValues = Oraculo.getHourValues(appliance, position);
    			txtHour.setText(hrValues.get(0)+":"+hrValues.get(1));
    			holder.serviceLayoutControl.addView(btnHour);
    			holder.serviceLayoutControl.addView(txtHour);
    			break;
    		case 10:
    			Button btnResponse = new Button(context);
    			holder.serviceLayoutControl.addView(btnResponse);
    			break;
    		case 11:
    			Button btnFile = new Button(context);
    			holder.serviceLayoutControl.addView(btnFile);
    			break;
    		case 12:
    			Button btnReturn = new Button(context);
    			holder.serviceLayoutControl.addView(btnReturn);
    			break;
    	}
    	
		return row;

	}
	
	static class RecordHolder {
		ImageView serviceImage;
		TextView serviceName;
		LinearLayout serviceLayoutControl;
	}

}
