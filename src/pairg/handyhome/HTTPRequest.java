package pairg.handyhome;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.CookieStore;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class HTTPRequest {
	// Minhas variáveis de classe
	private static String TAG = HTTPRequest.class.getName();
	private static ArrayList<NameValuePair> parametros = new ArrayList<NameValuePair>();
	private static String url = "http://10.9.99.46/HomeCore/ucore.php";

	
	
	
	
	
	public static Integer Login(String username, String password) {
		/*
		 * Login automático independente de servidor
		 */
		if((username.contains("U")) && (password.contains("P"))){
			return 1;
		}else return 0;
		/*
		parametros.clear();
		parametros.add(new BasicNameValuePair("method","do_login"));
		parametros.add(new BasicNameValuePair("username", username));
		parametros.add(new BasicNameValuePair("password", password));
		
		// TODO Auto-generated method stub
		try{			
			DefaultHttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(url);
			// Cookies
			CookieStore cookieStore = new BasicCookieStore();
			
			// Contexto
			HttpContext localContext = new BasicHttpContext();
			
			// Link cookies -> contexto
			localContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
			
			// Coloca os parâmetros no cabeçalho da requisição
			post.setEntity(new UrlEncodedFormEntity(parametros));
			Log.i(TAG, "Entidade setada");
			
			// Envia a requisição e calcula o tempo de resposta
			HttpResponse response = (HttpResponse) client.execute(post, localContext);
			
			Log.i(TAG, "Recebeu a resposta");
			
			// Captura os dados da resposta
			HttpEntity entity = response.getEntity();
	
			if (entity != null) {
				// L� o conte�do da stream
				InputStream instream = entity.getContent();
	
				// Converte o conte�do para uma String
				String resultString = HTTPRequest.convertStreamToString(instream);
				Log.i(TAG, "JSONObject -> " + resultString);
				instream.close();
				
				// String -> JSONObject
				JSONObject jsonObjRecv = new JSONObject(resultString);
				
				return (Integer) (jsonObjRecv.get("status"));
			}	
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;	
		*/
	}// fim Login
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//METODO GET GROUPS, OBTER GRUPOS (HOME, APPLIANCES OU OTHERS)
	public static JSONObject getGroups(String nome) throws JSONException{
	//Metodo para obter os grupos a serem mostrados nas tabs (de home, de appliances ou de others)
	//Entrada: String com o nome da tab correspondente a ser preenchida com os grupos
	Log.i("TAG","HttpRequest getGroups()");
	
		/*parametros.clear();
		if(nome.equals("home")){
			parametros.add(new BasicNameValuePair("method","gather_home_groups"));
		}
		else{
			if(nome.equals("appliances")){
				parametros.add(new BasicNameValuePair("method","gather_appliances_groups"));
			}
			else{
				if(nome.equals("others")){
					parametros.add(new BasicNameValuePair("method","gather_user_groups"));
				}
			}
		}
		JSONObject myjson = connectToSytem();
		return myjson;*/
	
		//ESTATICO 2013
		//JSONObject teste = new JSONObject("{\"status\":\"1\",\"message\":\"Opera��o conclu�da com sucesso!\",\"content\":[{\"kind\":\"HomeGroups\",\"id\":\"10\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Sala\",\"image\":\"files/uploads/imagem1.png\"},\"objects\":7},{\"kind\":\"HomeGroups\",\"id\":\"11\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"4\",\"name\":\"Quarto\",\"image\":\"files/uploads/imagem2.png\"},\"objects\":5},{\"kind\":\"HomeGroups\",\"id\":\"12\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"5\",\"name\":\"Banheiro\",\"image\":\"files/uploads/imagem3.png\"},\"objects\":3},{\"kind\":\"HomeGroups\",\"id\":\"10\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Sala\",\"image\":\"files/uploads/imagem1.png\"},\"objects\":7},{\"kind\":\"HomeGroups\",\"id\":\"11\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"4\",\"name\":\"Quarto\",\"image\":\"files/uploads/imagem2.png\"},\"objects\":5},{\"kind\":\"HomeGroups\",\"id\":\"12\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"5\",\"name\":\"Banheiro\",\"image\":\"files/uploads/imagem3.png\"},\"objects\":3},{\"kind\":\"HomeGroups\",\"id\":\"10\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Sala\",\"image\":\"files/uploads/imagem1.png\"},\"objects\":7},{\"kind\":\"HomeGroups\",\"id\":\"11\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"4\",\"name\":\"Quarto\",\"image\":\"files/uploads/imagem2.png\"},\"objects\":5},{\"kind\":\"HomeGroups\",\"id\":\"12\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"5\",\"name\":\"Banheiro\",\"image\":\"files/uploads/imagem3.png\"},\"objects\":3},{\"kind\":\"HomeGroups\",\"id\":\"10\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Sala\",\"image\":\"files/uploads/imagem1.png\"},\"objects\":7},{\"kind\":\"HomeGroups\",\"id\":\"11\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"4\",\"name\":\"Quarto\",\"image\":\"files/uploads/imagem2.png\"},\"objects\":5},{\"kind\":\"HomeGroups\",\"id\":\"12\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"5\",\"name\":\"Banheiro\",\"image\":\"files/uploads/imagem3.png\"},\"objects\":3},{\"kind\":\"HomeGroups\",\"id\":\"10\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Sala\",\"image\":\"files/uploads/imagem1.png\"},\"objects\":7},{\"kind\":\"HomeGroups\",\"id\":\"11\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"4\",\"name\":\"Quarto\",\"image\":\"files/uploads/imagem2.png\"},\"objects\":5},{\"kind\":\"HomeGroups\",\"id\":\"12\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"5\",\"name\":\"Banheiro\",\"image\":\"files/uploads/imagem3.png\"},\"objects\":3},{\"kind\":\"HomeGroups\",\"id\":\"10\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Sala\",\"image\":\"files/uploads/imagem1.png\"},\"objects\":7},{\"kind\":\"HomeGroups\",\"id\":\"11\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"4\",\"name\":\"Quarto\",\"image\":\"files/uploads/imagem2.png\"},\"objects\":5},{\"kind\":\"HomeGroups\",\"id\":\"12\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"5\",\"name\":\"Banheiro\",\"image\":\"files/uploads/imagem3.png\"},\"objects\":3},{\"kind\":\"HomeGroups\",\"id\":\"10\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Sala\",\"image\":\"files/uploads/imagem1.png\"},\"objects\":7},{\"kind\":\"HomeGroups\",\"id\":\"11\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"4\",\"name\":\"Quarto\",\"image\":\"files/uploads/imagem2.png\"},\"objects\":5},{\"kind\":\"HomeGroups\",\"id\":\"12\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"5\",\"name\":\"Banheiro\",\"image\":\"files/uploads/imagem3.png\"},\"objects\":3},{\"kind\":\"HomeGroups\",\"id\":\"10\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Sala\",\"image\":\"files/uploads/imagem1.png\"},\"objects\":7},{\"kind\":\"HomeGroups\",\"id\":\"11\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"4\",\"name\":\"Quarto\",\"image\":\"files/uploads/imagem2.png\"},\"objects\":5},{\"kind\":\"HomeGroups\",\"id\":\"12\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"5\",\"name\":\"Banheiro\",\"image\":\"files/uploads/imagem3.png\"},\"objects\":3}]}");
		//return teste;
	
		//ESTATICO v2
		JSONObject teste = new JSONObject("{\"status\": \"0\"}");
		if(nome.equals("home")){
			teste = new JSONObject("{\"status\":\"1\",\"message\":\"Opera��o conclu�da com sucesso!\",\"content\":[{\"kind\":\"HomeGroups\",\"id\":\"101\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Sala\",\"image\":\"files/uploads/sala1.png\"},\"objects\":10},{\"kind\":\"HomeGroups\",\"id\":\"102\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"4\",\"name\":\"Quarto\",\"image\":\"files/uploads/quarto1.png\"},\"objects\":5},{\"kind\":\"HomeGroups\",\"id\":\"103\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"5\",\"name\":\"Banheiro\",\"image\":\"files/uploads/banheiro1.png\"},\"objects\":3},{\"kind\":\"HomeGroups\",\"id\":\"104\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Cozinha\",\"image\":\"files/uploads/cozinha1.png\"},\"objects\":5},{\"kind\":\"HomeGroups\",\"id\":\"105\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Varanda\",\"image\":\"files/uploads/varanda1.png\"},\"objects\":1},{\"kind\":\"HomeGroups\",\"id\":\"106\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Lavanderia\",\"image\":\"files/uploads/lavanderia1.png\"},\"objects\":2},{\"kind\":\"HomeGroups\",\"id\":\"107\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Biblioteca\",\"image\":\"files/uploads/biblioteca1.png\"},\"objects\":3},{\"kind\":\"HomeGroups\",\"id\":\"108\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Escrit�rio\",\"image\":\"files/uploads/escritorio1.png\"},\"objects\":7},{\"kind\":\"HomeGroups\",\"id\":\"109\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Garagem\",\"image\":\"files/uploads/garagem1.png\"},\"objects\":2},{\"kind\":\"HomeGroups\",\"id\":\"110\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Quarto B\",\"image\":\"files/uploads/quartobeliche1.png\"},\"objects\":1},{\"kind\":\"HomeGroups\",\"id\":\"111\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Pais\",\"image\":\"files/uploads/quartocasal1.png\"},\"objects\":5},{\"kind\":\"HomeGroups\",\"id\":\"112\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Av�s\",\"image\":\"files/uploads/quartocasal2.png\"},\"objects\":2},{\"kind\":\"HomeGroups\",\"id\":\"113\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Sala Estudo\",\"image\":\"files/uploads/biblioteca2.png\"},\"objects\":3},{\"kind\":\"HomeGroups\",\"id\":\"114\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Sala Leitura\",\"image\":\"files/uploads/sala2.png\"},\"objects\":3},{\"kind\":\"HomeGroups\",\"id\":\"115\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Quarto C1\",\"image\":\"files/uploads/comoda1.png\"},\"objects\":7},{\"kind\":\"HomeGroups\",\"id\":\"116\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Quarto C2\",\"image\":\"files/uploads/comoda2.png\"},\"objects\":7},{\"kind\":\"HomeGroups\",\"id\":\"117\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Garagem2\",\"image\":\"files/uploads/garagem2.png\"},\"objects\":7},{\"kind\":\"HomeGroups\",\"id\":\"118\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Quarto 2\",\"image\":\"files/uploads/quarto2.png\"},\"objects\":7},{\"kind\":\"HomeGroups\",\"id\":\"119\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Sala e1\",\"image\":\"files/uploads/sala1.png\"},\"objects\":7},{\"kind\":\"HomeGroups\",\"id\":\"120\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Sala e2\",\"image\":\"files/uploads/sala1.png\"},\"objects\":7},{\"kind\":\"HomeGroups\",\"id\":\"121\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Sala e3\",\"image\":\"files/uploads/sala1.png\"},\"objects\":7},{\"kind\":\"HomeGroups\",\"id\":\"122\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Sala e4\",\"image\":\"files/uploads/sala1.png\"},\"objects\":7},{\"kind\":\"HomeGroups\",\"id\":\"123\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Sala e5\",\"image\":\"files/uploads/sala1.png\"},\"objects\":7},{\"kind\":\"HomeGroups\",\"id\":\"124\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Sala e6\",\"image\":\"files/uploads/sala1.png\"},\"objects\":7}]}");
		}
		else{
			if(nome.equals("appliances")){
				teste = new JSONObject("{\"status\":\"1\",\"message\":\"Opera��o conclu�da com sucesso!\",\"content\":[{\"kind\":\"AppliancesGroups\",\"id\":\"201\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"L�mpadas\",\"image\":\"files/uploads/lampadas1.png\"},\"objects\":7},{\"kind\":\"AppliancesGroups\",\"id\":\"202\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"4\",\"name\":\"Portas\",\"image\":\"files/uploads/portas1.png\"},\"objects\":5},{\"kind\":\"AppliancesGroups\",\"id\":\"203\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"5\",\"name\":\"Janelas\",\"image\":\"files/uploads/janelas1.png\"},\"objects\":3},{\"kind\":\"AppliancesGroups\",\"id\":\"204\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Chuveiros\",\"image\":\"files/uploads/chuveiros1.png\"},\"objects\":5},{\"kind\":\"AppliancesGroups\",\"id\":\"205\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Ar Conds\",\"image\":\"files/uploads/arcondicionado1.png\"},\"objects\":1},{\"kind\":\"AppliancesGroups\",\"id\":\"206\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Luzes\",\"image\":\"files/uploads/luminaria1.png\"},\"objects\":2},{\"kind\":\"AppliancesGroups\",\"id\":\"207\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Outros 07\",\"image\":\"files/uploads/default.png\"},\"objects\":3},{\"kind\":\"AppliancesGroups\",\"id\":\"208\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Outros 08\",\"image\":\"files/uploads/default.png\"},\"objects\":7},{\"kind\":\"AppliancesGroups\",\"id\":\"209\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Outros 09\",\"image\":\"files/uploads/default.png\"},\"objects\":2},{\"kind\":\"AppliancesGroups\",\"id\":\"210\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Outros 10\",\"image\":\"files/uploads/default.png\"},\"objects\":1},{\"kind\":\"AppliancesGroups\",\"id\":\"211\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Outros 11\",\"image\":\"files/uploads/default.png\"},\"objects\":5},{\"kind\":\"AppliancesGroups\",\"id\":\"212\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Outros 12\",\"image\":\"files/uploads/default.png\"},\"objects\":2},{\"kind\":\"AppliancesGroups\",\"id\":\"213\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Outros 13\",\"image\":\"files/uploads/default.png\"},\"objects\":3},{\"kind\":\"AppliancesGroups\",\"id\":\"214\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Outros 14\",\"image\":\"files/uploads/default.png\"},\"objects\":3},{\"kind\":\"AppliancesGroups\",\"id\":\"215\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Outros 15\",\"image\":\"files/uploads/default.png\"},\"objects\":7},{\"kind\":\"AppliancesGroups\",\"id\":\"216\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Outros 16\",\"image\":\"files/uploads/default.png\"},\"objects\":7},{\"kind\":\"AppliancesGroups\",\"id\":\"217\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Outros 17\",\"image\":\"files/uploads/default.png\"},\"objects\":7},{\"kind\":\"AppliancesGroups\",\"id\":\"218\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Outros 18\",\"image\":\"files/uploads/default.png\"},\"objects\":7},{\"kind\":\"AppliancesGroups\",\"id\":\"219\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Outros 19\",\"image\":\"files/uploads/default.png\"},\"objects\":7},{\"kind\":\"AppliancesGroups\",\"id\":\"220\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Outros 20\",\"image\":\"files/uploads/default.png\"},\"objects\":7}]}");
			}
			else{
				if(nome.equals("others")){
					teste = new JSONObject("{\"status\":\"1\",\"message\":\"Opera��o conclu�da com sucesso!\",\"content\":[{\"kind\":\"UserGroups\",\"id\":\"301\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Grupo 1\",\"image\":\"files/uploads/varanda1.png\"},\"objects\":10},{\"kind\":\"UserGroups\",\"id\":\"302\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"4\",\"name\":\"Grupo 2\",\"image\":\"files/uploads/default.png\"},\"objects\":4},{\"kind\":\"UserGroups\",\"id\":\"303\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"5\",\"name\":\"Grupo 3\",\"image\":\"files/uploads/default.png\"},\"objects\":1},{\"kind\":\"UserGroups\",\"id\":\"304\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Grupo 4\",\"image\":\"files/uploads/default.png\"},\"objects\":2},{\"kind\":\"UserGroups\",\"id\":\"305\",\"owner\":\"0\",\"reg_time\":\"2013-10-15 15:18:03\",\"visual\":{\"id\":\"3\",\"name\":\"Grupo 5\",\"image\":\"files/uploads/default.png\"},\"objects\":3}]}");
				}
			}
		}
		
		return teste;
	}
	
	
	
	//METODO GET OBJECTS, OBTER OBJETOS DE UM DETERMINADO GRUPO
	public static JSONObject getObjects(String id) throws JSONException{
	//Metodo para obter os objetos de um determinado grupo
	//Entrada: String com o id do grupo
	Log.i("TAG","HttpRequest getObjects()");
		
		/*parametros.clear();
		parametros.add(new BasicNameValuePair("method","gather_group"));
		JSONObject myjson = connectToSytem();
		return myjson;*/
		
		
		//ESTATICO v2
		JSONObject teste = new JSONObject("{\"status\": \"0\"}");
		if(id.equals("101")){
			teste = new JSONObject("{\"status\":\"1\",\"message\":\"Operacao concluida\",\"content\":[{\"kind\":\"HObject\",\"id\":\"1\",\"type\":\"lamp\",\"reg_time\":\"2013/09/23 13:58:43\",\"validated\":\"1\",\"connected\":\"0\",\"visual\":{\"id\":\"7\",\"name\":\"Admin Lamp 1\",\"image\":\"files/uploads/lampada_off.png\"},\"services\":[{\"id\":\"==QTR1TP\",\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"101\"],\"input_type\":[\"boolean\"],\"status_id\":[\"1\"]},{\"id\":\"==QTn1TP\",\"name\":\"travar/destravar\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"102\"],\"input_type\":[\"boolean\"],\"status_id\":[\"2\"]},{\"id\":\"==QT31TP\",\"name\":\"iluminacao\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"103\"],\"input_type\":[\"int_range_granular\"],\"status_id\":[\"3\"]},{\"id\":\"==QT31TP\",\"name\":\"temperatura\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"104\"],\"input_type\":[\"int_range_gradual\"],\"status_id\":[\"4\"]},{\"id\":\"==QT31TP\",\"name\":\"selecionar modo\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"105\"],\"input_type\":[\"option\"],\"status_id\":[\"5\"]},{\"id\":\"==QT31TP\",\"name\":\"alterar data\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"106\"],\"input_type\":[\"date\"],\"status_id\":[\"6\"]},{\"id\":\"==QT31TP\",\"name\":\"alterar hora\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"107\"],\"input_type\":[\"hour\"],\"status_id\":[\"7\"]},{\"id\":\"==QT31TP\",\"name\":\"lavar\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"108\",\"109\",\"110\",\"111\",\"112\",\"113\"],\"input_type\":[\"int_range_granular\",\"option\",\"boolean\",\"boolean\",\"option\",\"string\"],\"status_id\":[\"8\",\"9\",\"10\",\"11\",\"12\",null],\"names\":[\"n�vel de �gua\",\"modo de opera��o\",\"amaciante\",\"turbo\",\"secagem\",\"mensagem\"]},{\"id\":\"==QT31TP\",\"name\":\"enviar mensagem\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"114\"],\"input_type\":[\"string\"],\"status_id\":[null]},{\"id\":\"==QT31TP\",\"name\":\"alterar mensagem\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"115\"],\"input_type\":[\"string\"],\"status_id\":[\"13\"]},{\"id\":\"==QT31TP\",\"name\":\"alterar imagem\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"116\"],\"input_type\":[\"option\"],\"status_id\":[\"14\"]},{\"id\":\"==QT31TP\",\"name\":\"exibir arquivo no dispositivo\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"117\"],\"input_type\":[\"file\"],\"status_id\":[null]},{\"id\":\"==QT31TP\",\"name\":\"lavar executar\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[null],\"input_type\":[null],\"status_id\":[null]},{\"id\":\"==QT31TP\",\"name\":\"lavar e alterar status\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[null],\"input_type\":[null],\"status_id\":[\"15\"]},{\"id\":\"==QT31TP\",\"name\":\"mostrar v�deo para sarah que ela eh muito legal eeeee\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[null],\"input_type\":[null],\"status_id\":[null],\"return_type\":[\"file-video\"]}],\"status\":[{\"id\":\"1\",\"type\":\"boolean\",\"value\":true,\"name_false\":\"aberta\",\"name_true\":\"fechada\"},{\"id\":\"2\",\"type\":\"boolean\",\"value\":false,\"name_false\":\"destravada\",\"name_true\":\"travada\"},{\"id\":\"3\",\"type\":\"int_range_granular\",\"value\":10,\"range\":[0,100]},{\"id\":\"4\",\"type\":\"int_range_gradual\",\"value\":25,\"range\":[17,27],\"principal\":1},{\"id\":\"5\",\"type\":\"option\",\"value\":\"modo1\",\"options\":[\"modo1\",\"modo2\",\"modo3\"]},{\"id\":\"6\",\"type\":\"date\",\"value\":{\"day\":10,\"month\":4,\"year\":2013}},{\"id\":\"7\",\"type\":\"hour\",\"value\":{\"hour\":12,\"minute\":15},\"principal\":2},{\"id\":\"8\",\"type\":\"int_range_granular\",\"value\":0,\"range\":[0,5]},{\"id\":\"9\",\"type\":\"option\",\"value\":\"molho longo\",\"options\":[\"molho longo\",\"molho curto\",\"lavar\",\"enxaguar\",\"centrifugar\"]},{\"id\":\"10\",\"type\":\"boolean\",\"value\":false,\"name_true\":\"com turbo\",\"name_false\":\"sem turbo\"},{\"id\":\"11\",\"type\":\"boolean\",\"value\":false,\"name_true\":\"ativado\",\"name_false\":\"desativado\"},{\"id\":\"12\",\"type\":\"option\",\"value\":\"nenhuma\",\"options\":[\"nenhuma\",\"econ�mica\",\"normal\"]},{\"id\":\"13\",\"type\":\"string\",\"value\":\"ol�! essa mensagem padr�o est� sendo exibida.\"},{\"id\":\"14\",\"type\":\"option\",\"value\":\"foto1.jpg\",\"options\":[\"foto1.jpg\",\"foto2.jpg\",\"foto3.jpg\",\"foto4.jpg\",\"foto5.jpg\",\"foto6.jpg\",\"foto7.jpg\",\"foto8.jpg\",\"foto9.jpg\",\"foto10.jpg\",\"foto11.jpg\",\"foto12.jpg\"]},{\"id\":\"15\",\"type\":\"string\",\"value\":\"lavando\"}]},{\"kind\":\"HObject\",\"id\":\"2\",\"type\":\"lamp\",\"reg_time\":\"2013/09/23 13:58:43\",\"validated\":\"1\",\"connected\":\"0\",\"visual\":{\"id\":\"7\",\"name\":\"Admin Lamp 2\",\"image\":\"files/uploads/lampada_on.png\"},\"services\":[{\"id\":\"==QT31TP\",\"name\":\"abrir\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[null],\"input_type\":[null],\"status_id\":[\"1\"]},{\"id\":\"==QT31TP\",\"name\":\"fechar\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[null],\"input_type\":[null],\"status_id\":[\"1\"]},{\"id\":\"==QT31TP\",\"name\":\"travar/destravar\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"118\"],\"input_type\":[\"boolean\"],\"status_id\":[\"2\"]},{\"id\":\"==gTB1TP\",\"name\":\"executar\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[null],\"input_type\":[null],\"status_id\":[null]}],\"status\":[{\"id\":\"1\",\"type\":\"boolean\",\"value\":true,\"name_false\":\"aberta\",\"name_true\":\"fechada\"},{\"id\":\"2\",\"type\":\"boolean\",\"value\":true,\"name_false\":\"travada\",\"name_true\":\"destravada\"}]},{\"kind\":\"HObject\",\"id\":\"3\",\"type\":\"window\",\"reg_time\":\"2013/09/23 13:58:43\",\"validated\":\"1\",\"connected\":\"0\",\"visual\":{\"id\":\"7\",\"name\":\"Janela\",\"image\":\"files/uploads/janela_open.png\"},\"services\":[{\"id\":\"==QTR1TP\",\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"119\"],\"input_type\":[\"boolean\"],\"status_id\":[\"1\"]}],\"status\":[{\"id\":\"1\",\"type\":\"boolean\",\"value\":true,\"name_false\":\"aberta\",\"name_true\":\"fechada\"}]},{\"kind\":\"HObject\",\"id\":\"4\",\"type\":\"door\",\"reg_time\":\"2013/09/23 13:58:43\",\"validated\":\"1\",\"connected\":\"0\",\"visual\":{\"id\":\"7\",\"name\":\"Porta\",\"image\":\"files/uploads/porta_open.png\"},\"services\":[{\"id\":\"==QTR1TP\",\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"120\"],\"input_type\":[\"boolean\"],\"status_id\":[\"1\"]}],\"status\":[{\"id\":\"1\",\"type\":\"boolean\",\"value\":true,\"name_false\":\"aberta\",\"name_true\":\"fechada\"}]},{\"kind\":\"HObject\",\"id\":\"5\",\"type\":\"lamp\",\"reg_time\":\"2013/09/23 13:58:43\",\"validated\":\"1\",\"connected\":\"0\",\"visual\":{\"id\":\"7\",\"name\":\"Lamp 5\",\"image\":\"files/uploads/lampada_off.png\"},\"services\":[{\"id\":\"==QTR1TP\",\"name\":\"ligar/desligar\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"121\"],\"input_type\":[\"boolean\"],\"status_id\":[\"1\"]}],\"status\":[{\"id\":\"1\",\"type\":\"boolean\",\"value\":true,\"name_false\":\"ligada\",\"name_true\":\"desligada\"}]},{\"kind\":\"HObject\",\"id\":\"6\",\"type\":\"lamp\",\"reg_time\":\"2013/09/23 13:58:43\",\"validated\":\"1\",\"connected\":\"0\",\"visual\":{\"id\":\"7\",\"name\":\"Lamp 6\",\"image\":\"files/uploads/lampada_off.png\"},\"services\":[{\"id\":\"==QTR1TP\",\"name\":\"ligar/desligar\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"122\"],\"input_type\":[\"boolean\"],\"status_id\":[\"1\"]}],\"status\":[{\"id\":\"1\",\"type\":\"boolean\",\"value\":true,\"name_false\":\"ligada\",\"name_true\":\"desligada\"}]},{\"kind\":\"HObject\",\"id\":\"7\",\"type\":\"lamp\",\"reg_time\":\"2013/09/23 13:58:43\",\"validated\":\"1\",\"connected\":\"0\",\"visual\":{\"id\":\"7\",\"name\":\"Lamp 7\",\"image\":\"files/uploads/lampada_off.png\"},\"services\":[{\"id\":\"==QTR1TP\",\"name\":\"ligar/desligar\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"123\"],\"input_type\":[\"boolean\"],\"status_id\":[\"1\"]}],\"status\":[{\"id\":\"1\",\"type\":\"boolean\",\"value\":true,\"name_false\":\"ligada\",\"name_true\":\"desligada\"}]},{\"kind\":\"HObject\",\"id\":\"8\",\"type\":\"lamp\",\"reg_time\":\"2013/09/23 13:58:43\",\"validated\":\"1\",\"connected\":\"0\",\"visual\":{\"id\":\"7\",\"name\":\"Lamp 8\",\"image\":\"files/uploads/lampada_off.png\"},\"services\":[{\"id\":\"==QTR1TP\",\"name\":\"ligar/desligar\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"124\"],\"input_type\":[\"boolean\"],\"status_id\":[\"1\"]}],\"status\":[{\"id\":\"1\",\"type\":\"boolean\",\"value\":true,\"name_false\":\"ligada\",\"name_true\":\"desligada\"}]},{\"kind\":\"HObject\",\"id\":\"9\",\"type\":\"lamp\",\"reg_time\":\"2013/09/23 13:58:43\",\"validated\":\"1\",\"connected\":\"0\",\"visual\":{\"id\":\"7\",\"name\":\"Lamp 9\",\"image\":\"files/uploads/lampada_off.png\"},\"services\":[{\"id\":\"==QTR1TP\",\"name\":\"ligar/desligar\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"125\"],\"input_type\":[\"boolean\"],\"status_id\":[\"1\"]}],\"status\":[{\"id\":\"1\",\"type\":\"boolean\",\"value\":true,\"name_false\":\"ligada\",\"name_true\":\"desligada\"}]},{\"kind\":\"HObject\",\"id\":\"10\",\"type\":\"lamp\",\"reg_time\":\"2013/09/23 13:58:43\",\"validated\":\"1\",\"connected\":\"0\",\"visual\":{\"id\":\"7\",\"name\":\"Lamp 10\",\"image\":\"files/uploads/lampada_off.png\"},\"services\":[{\"id\":\"==QTR1TP\",\"name\":\"ligar/desligar\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"126\"],\"input_type\":[\"boolean\"],\"status_id\":[\"1\"]}],\"status\":[{\"id\":\"1\",\"type\":\"boolean\",\"value\":true,\"name_false\":\"ligada\",\"name_true\":\"desligada\"}]}]}");
		}
		else{
			teste = new JSONObject("{\"status\":\"1\",\"message\":\"Operacao concluida\",\"content\":[{\"kind\":\"HObject\",\"id\":\"1\",\"type\":\"door\",\"reg_time\":\"2013/09/23 13:58:43\",\"validated\":\"1\",\"connected\":\"0\",\"visual\":{\"id\":\"7\",\"name\":\"Admin Lamp 1\",\"image\":null},\"services\":[{\"id\":\"==QTR1TP\",\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"101\"],\"input_type\":[\"boolean\"],\"status_id\":[\"1\"]},{\"id\":\"==QTn1TP\",\"name\":\"travar/destravar\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"102\"],\"input_type\":[\"boolean\"],\"status_id\":[\"2\"]},{\"id\":\"==QT31TP\",\"name\":\"iluminacao\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"103\"],\"input_type\":[\"int_range_granular\"],\"status_id\":[\"3\"]},{\"id\":\"==QT31TP\",\"name\":\"temperatura\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"104\"],\"input_type\":[\"int_range_gradual\"],\"status_id\":[\"4\"]},{\"id\":\"==QT31TP\",\"name\":\"selecionar modo\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"105\"],\"input_type\":[\"option\"],\"status_id\":[\"5\"]},{\"id\":\"==QT31TP\",\"name\":\"alterar data\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"106\"],\"input_type\":[\"date\"],\"status_id\":[\"6\"]},{\"id\":\"==QT31TP\",\"name\":\"alterar hora\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"107\"],\"input_type\":[\"hour\"],\"status_id\":[\"7\"]},{\"id\":\"==QT31TP\",\"name\":\"lavar\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"108\",\"109\",\"110\",\"111\",\"112\",\"113\"],\"input_type\":[\"int_range_granular\",\"option\",\"boolean\",\"boolean\",\"option\",\"string\"],\"status_id\":[\"8\",\"9\",\"10\",\"11\",\"12\",null],\"names\":[\"n�vel de �gua\",\"modo de opera��o\",\"amaciante\",\"turbo\",\"secagem\",\"mensagem\"]},{\"id\":\"==QT31TP\",\"name\":\"enviar mensagem\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"114\"],\"input_type\":[\"string\"],\"status_id\":[null]},{\"id\":\"==QT31TP\",\"name\":\"alterar mensagem\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"115\"],\"input_type\":[\"string\"],\"status_id\":[\"13\"]},{\"id\":\"==QT31TP\",\"name\":\"alterar imagem\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"116\"],\"input_type\":[\"option\"],\"status_id\":[\"14\"]},{\"id\":\"==QT31TP\",\"name\":\"exibir arquivo no dispositivo\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"117\"],\"input_type\":[\"file\"],\"status_id\":[null]},{\"id\":\"==QT31TP\",\"name\":\"lavar executar\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[null],\"input_type\":[null],\"status_id\":[null]},{\"id\":\"==QT31TP\",\"name\":\"lavar e alterar status\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[null],\"input_type\":[null],\"status_id\":[\"15\"]},{\"id\":\"==QT31TP\",\"name\":\"mostrar v�deo\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[null],\"input_type\":[null],\"status_id\":[null],\"return_type\":[\"file-video\"]}],\"status\":[{\"id\":\"1\",\"type\":\"boolean\",\"value\":true,\"name_false\":\"aberta\",\"name_true\":\"fechada\"},{\"id\":\"2\",\"type\":\"boolean\",\"value\":false,\"name_false\":\"destravada\",\"name_true\":\"travada\"},{\"id\":\"3\",\"type\":\"int_range_granular\",\"value\":10,\"range\":[0,100]},{\"id\":\"4\",\"type\":\"int_range_gradual\",\"value\":25,\"range\":[17,27],\"principal\":1},{\"id\":\"5\",\"type\":\"option\",\"value\":\"modo1\",\"options\":[\"modo1\",\"modo2\",\"modo3\"]},{\"id\":\"6\",\"type\":\"date\",\"value\":{\"day\":10,\"month\":4,\"year\":2013}},{\"id\":\"7\",\"type\":\"hour\",\"value\":{\"hour\":12,\"minute\":15},\"principal\":2},{\"id\":\"8\",\"type\":\"int_range_granular\",\"value\":0,\"range\":[0,5]},{\"id\":\"9\",\"type\":\"option\",\"value\":\"molho longo\",\"options\":[\"molho longo\",\"molho curto\",\"lavar\",\"enxaguar\",\"centrifugar\"]},{\"id\":\"10\",\"type\":\"boolean\",\"value\":false,\"name_true\":\"com turbo\",\"name_false\":\"sem turbo\"},{\"id\":\"11\",\"type\":\"boolean\",\"value\":false,\"name_true\":\"ativado\",\"name_false\":\"desativado\"},{\"id\":\"12\",\"type\":\"option\",\"value\":\"nenhuma\",\"options\":[\"nenhuma\",\"econ�mica\",\"normal\"]},{\"id\":\"13\",\"type\":\"string\",\"value\":\"ol�! essa mensagem padr�o est� sendo exibida.\"},{\"id\":\"14\",\"type\":\"option\",\"value\":\"foto1.jpg\",\"options\":[\"foto1.jpg\",\"foto2.jpg\",\"foto3.jpg\",\"foto4.jpg\",\"foto5.jpg\",\"foto6.jpg\",\"foto7.jpg\",\"foto8.jpg\",\"foto9.jpg\",\"foto10.jpg\",\"foto11.jpg\",\"foto12.jpg\"]},{\"id\":\"15\",\"type\":\"string\",\"value\":\"lavando\"}]},{\"kind\":\"HObject\",\"id\":\"1\",\"type\":\"door\",\"reg_time\":\"2013/09/23 13:58:43\",\"validated\":\"1\",\"connected\":\"0\",\"visual\":{\"id\":\"7\",\"name\":\"Admin Lamp 2\",\"image\":null},\"services\":[{\"id\":\"==QT31TP\",\"name\":\"abrir\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[null],\"input_type\":[null],\"status_id\":[\"1\"]},{\"id\":\"==QT31TP\",\"name\":\"fechar\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[null],\"input_type\":[null],\"status_id\":[\"1\"]},{\"id\":\"==QT31TP\",\"name\":\"travar/destravar\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[\"118\"],\"input_type\":[\"boolean\"],\"status_id\":[\"2\"]},{\"id\":\"==gTB1TP\",\"name\":\"executar\",\"image\":\"files/uploads/imagemx.png\",\"input_id\":[null],\"input_type\":[null],\"status_id\":[null]}],\"status\":[{\"id\":\"1\",\"type\":\"boolean\",\"value\":true,\"name_false\":\"aberta\",\"name_true\":\"fechada\"},{\"id\":\"2\",\"type\":\"boolean\",\"value\":true,\"name_false\":\"travada\",\"name_true\":\"destravada\"}]}]}");
		}
		return teste;
	}
	
	
	
	//METODO GET SCENES
	public static JSONObject getScenes() throws JSONException{
		//Metodo para obter as cena
		//Entrada: -
		Log.i("TAG","HttpRequest getScenes()");
			
			/*parametros.clear();
			parametros.add(new BasicNameValuePair("method","gather_scenes"));
			JSONObject myjson = connectToSytem();
			return myjson;*/

			//ESTATICO v2
			//JSONObject teste = new JSONObject("{\"status\":\"1\",\"message\":\"Operacao concluida\",\"content\":[{\"kind\":\"Scene\",\"id\":\"1\",\"reg_time\":\"2013/09/23 13:58:43\",\"visual\":{\"id\":\"1\",\"name\":\"Sair de casa\",\"image\":\"files/uploads/ic_cena_sair.png\"},\"actions\":[{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"},{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"}]},{\"kind\":\"Scene\",\"id\":\"2\",\"reg_time\":\"2013/09/23 13:58:43\",\"visual\":{\"id\":\"2\",\"name\":\"Chovendo\",\"image\":\"files/uploads/ic_cena_chovendo.png\"},\"actions\":[{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"},{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"}]},{\"kind\":\"Scene\",\"id\":\"3\",\"reg_time\":\"2013/09/23 13:58:43\",\"visual\":{\"id\":\"3\",\"name\":\"Acordar\",\"image\":\"files/uploads/ic_cena_acordar.png\"},\"actions\":[{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"},{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"}]},{\"kind\":\"Scene\",\"id\":\"4\",\"reg_time\":\"2013/09/23 13:58:43\",\"visual\":{\"id\":\"4\",\"name\":\"Lavar roupa\",\"image\":\"files/uploads/ic_cena_lavarroupa.png\"},\"actions\":[{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"},{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"}]},{\"kind\":\"Scene\",\"id\":\"5\",\"reg_time\":\"2013/09/23 13:58:43\",\"visual\":{\"id\":\"5\",\"name\":\"Antes de dormir\",\"image\":\"files/uploads/ic_cena_dormir.png\"},\"actions\":[{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"},{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"}]},{\"kind\":\"Scene\",\"id\":\"6\",\"reg_time\":\"2013/09/23 13:58:43\",\"visual\":{\"id\":\"6\",\"name\":\"Saindo de viagem\",\"image\":\"files/uploads/ic_cena_viagem.png\"},\"actions\":[{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"},{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"}]}]}");
			JSONObject teste = new JSONObject("{\"status\":\"1\",\"message\":\"Operacao concluida\",\"content\":[{\"kind\":\"Scene\",\"id\":\"1\",\"reg_time\":\"2013/09/23 13:58:43\",\"visual\":{\"id\":\"1\",\"name\":\"Acordar\",\"image\":\"files/uploads/ic_cena_acordar.png\"},\"actions\":[{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"},{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"}]},{\"kind\":\"Scene\",\"id\":\"2\",\"reg_time\":\"2013/09/23 13:58:43\",\"visual\":{\"id\":\"2\",\"name\":\"Sair de casa\",\"image\":\"files/uploads/ic_cena_sair.png\"},\"actions\":[{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"},{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"}]},{\"kind\":\"Scene\",\"id\":\"3\",\"reg_time\":\"2013/09/23 13:58:43\",\"visual\":{\"id\":\"3\",\"name\":\"Chovendo\",\"image\":\"files/uploads/ic_cena_chovendo.png\"},\"actions\":[{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"},{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"}]},{\"kind\":\"Scene\",\"id\":\"4\",\"reg_time\":\"2013/09/23 13:58:43\",\"visual\":{\"id\":\"4\",\"name\":\"Lavar roupa\",\"image\":\"files/uploads/ic_cena_lavarroupa.png\"},\"actions\":[{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"},{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"}]},{\"kind\":\"Scene\",\"id\":\"5\",\"reg_time\":\"2013/09/23 13:58:43\",\"visual\":{\"id\":\"5\",\"name\":\"Dia de secar roupa\",\"image\":\"files/uploads/ic_cena_secarroupa.png\"},\"actions\":[{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"},{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"}]},{\"kind\":\"Scene\",\"id\":\"6\",\"reg_time\":\"2013/09/23 13:58:43\",\"visual\":{\"id\":\"6\",\"name\":\"Cheguei do trabalho\",\"image\":\"files/uploads/ic_cena_trabalho.png\"},\"actions\":[{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"},{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"}]},{\"kind\":\"Scene\",\"id\":\"7\",\"reg_time\":\"2013/09/23 13:58:43\",\"visual\":{\"id\":\"7\",\"name\":\"Banho\",\"image\":\"files/uploads/ic_cena_banho.png\"},\"actions\":[{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"},{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"}]},{\"kind\":\"Scene\",\"id\":\"8\",\"reg_time\":\"2013/09/23 13:58:43\",\"visual\":{\"id\":\"8\",\"name\":\"Saindo de viagem\",\"image\":\"files/uploads/ic_cena_viagem.png\"},\"actions\":[{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"},{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"}]},{\"kind\":\"Scene\",\"id\":\"9\",\"reg_time\":\"2013/09/23 13:58:43\",\"visual\":{\"id\":\"9\",\"name\":\"Dia de festa\",\"image\":\"files/uploads/ic_cena_festa.png\"},\"actions\":[{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"},{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"}]},{\"kind\":\"Scene\",\"id\":\"10\",\"reg_time\":\"2013/09/23 13:58:43\",\"visual\":{\"id\":\"10\",\"name\":\"Cinema em casa\",\"image\":\"files/uploads/ic_cena_cinema.png\"},\"actions\":[{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"},{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"}]},{\"kind\":\"Scene\",\"id\":\"11\",\"reg_time\":\"2013/09/23 13:58:43\",\"visual\":{\"id\":\"11\",\"name\":\"Tocar m�sicas\",\"image\":\"files/uploads/ic_cena_musica.png\"},\"actions\":[{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"},{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"}]},{\"kind\":\"Scene\",\"id\":\"12\",\"reg_time\":\"2013/09/23 13:58:43\",\"visual\":{\"id\":\"12\",\"name\":\"Antes de dormir\",\"image\":\"files/uploads/ic_cena_dormir.png\"},\"actions\":[{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"},{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"}]}]}");
			return teste;
		}
	
	//METODO GET FAVORITE SCENES
	public static JSONObject getFavoriteScenes() throws JSONException{
		//Metodo para obter as cena
		//Entrada: -
		Log.i("TAG","HttpRequest getFavoriteScenes()");
			
			/*parametros.clear();
			parametros.add(new BasicNameValuePair("method","gather_scenes"));
			JSONObject myjson = connectToSytem();
			return myjson;*/

			//ESTATICO v2
			JSONObject teste = new JSONObject("{\"status\":\"1\",\"message\":\"Operacao concluida\",\"content\":[{\"kind\":\"Scene\",\"id\":\"1\",\"reg_time\":\"2013/09/23 13:58:43\",\"visual\":{\"id\":\"1\",\"name\":\"Acordar\",\"image\":\"files/uploads/ic_cena_acordar.png\"},\"actions\":[{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"},{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"}]},{\"kind\":\"Scene\",\"id\":\"12\",\"reg_time\":\"2013/09/23 13:58:43\",\"visual\":{\"id\":\"12\",\"name\":\"Antes de dormir\",\"image\":\"files/uploads/ic_cena_dormir.png\"},\"actions\":[{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"},{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"}]},{\"kind\":\"Scene\",\"id\":\"7\",\"reg_time\":\"2013/09/23 13:58:43\",\"visual\":{\"id\":\"7\",\"name\":\"Banho\",\"image\":\"files/uploads/ic_cena_banho.png\"},\"actions\":[{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"},{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"}]},{\"kind\":\"Scene\",\"id\":\"2\",\"reg_time\":\"2013/09/23 13:58:43\",\"visual\":{\"id\":\"2\",\"name\":\"Sair de casa\",\"image\":\"files/uploads/ic_cena_sair.png\"},\"actions\":[{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"},{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"}]},{\"kind\":\"Scene\",\"id\":\"3\",\"reg_time\":\"2013/09/23 13:58:43\",\"visual\":{\"id\":\"3\",\"name\":\"Chovendo\",\"image\":\"files/uploads/ic_cena_chovendo.png\"},\"actions\":[{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"},{\"name\":\"abrir/fechar\",\"image\":\"files/uploads/imagemx.png\"}]}]}");
			return teste;
		}
	
	
	// METODO GET_USER_BASIC_INFO (JSON a ser completo)
	public static JSONObject getUserBasicInfo() throws JSONException{
		//ESTATICO
		JSONObject teste = new JSONObject("{\"status\": \"0\"}");
		return teste;
	}
	
	// METODO GET_USER_INFO (JSON a ser completo)
	public static JSONObject getUserInfo() throws JSONException{
		//ESTATICO
		JSONObject teste = new JSONObject("{\"status\": \"0\"}");
		return teste;
	}
	
	
	
	
	
	
	public static void logoff(){
		//desconectar
		return;
	}
	
	public static void closeApp(){
		//android.os.Process.killProcess(android.os.Process.myPid());
		//System.runFinalizersOnExit(true);
	}
	
	
	public static JSONObject connectToSytem(){
	//Metodo de conexao com servidor, � chamado por outros m�todos
		JSONObject x = new JSONObject();
		return x;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	public static String convertStreamToString(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}// fim convertStreamToString
	
	public static JSONObject httpRequest(String method){

		parametros.clear();
		parametros.add(new BasicNameValuePair("method", method));
		
		// TODO Auto-generated method stub
		try{			
			DefaultHttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(url);
			// Cookies
			CookieStore cookieStore = (CookieStore) new BasicCookieStore();
			
			// Contexto
			HttpContext localContext = new BasicHttpContext();
			
			// Link cookies -> contexto
			localContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
			
			// Coloca os parâmetros no cabeçalho da requisição
			post.setEntity(new UrlEncodedFormEntity(parametros));
			Log.i(TAG, "Entidade setada");
			
			// Envia a requisição e calcula o tempo de resposta
			HttpResponse response = client.execute(post, localContext);
			
			Log.i(TAG, "Recebeu a resposta");
			
			// Captura os dados da resposta
			HttpEntity entity = response.getEntity();
	
			if (entity != null) {
				// L� o conte�do da stream
				InputStream instream = entity.getContent();
	
				// Converte o conte�do para uma String
				String resultString = HTTPRequest.convertStreamToString(instream);
				Log.i(TAG, "JSONObject -> " + resultString);
				instream.close();
				
				// String -> JSONObject
				JSONObject jsonObjRecv = new JSONObject(resultString);
				
				return (JSONObject) (jsonObjRecv.get("status"));
			}	
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;	

	}
	
	
	
	
	
}
