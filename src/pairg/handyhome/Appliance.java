package pairg.handyhome;

import java.util.ArrayList;

//import java.io.Serializable;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Bitmap;
import android.util.Log;

public class Appliance {
	// atributos
	private String id;
	private String kind;
	private String type;
	private String visual_id;
	private String visual_name;
	private Bitmap visual_image;
	private ArrayList<Service> services = new ArrayList<Service>();
	private ArrayList<Status> status = new ArrayList<Status>();
	private boolean hasprincipal1 = false;
	private boolean hasprincipal2 = false;
	private String principal1 = new String(); //id do status que � principal1
	private String principal2 = new String(); //id do status que � principal2
	
	// construtor
	public Appliance(String id, String kind, String type, String visual_id, String visual_name, Bitmap visual_image, JSONArray services, JSONArray status) throws JSONException{
		Log.i("APPLIANCE", "ENTREI NO CONSTRUTOR!");		
		this.kind = kind;
		this.id = id;
		this.type = type;
		this.visual_id = visual_id;
		this.visual_name = visual_name;
		this.visual_image = visual_image;
		this.services = mapJSONArraytoServices(services);
		this.status = mapJSONArraytoStatus(status);
		
		//procurar e preencher principal1 e principal2 com os IDs dos status
		for(int i = 0; i < this.status.size(); i++){
			if (this.status.get(i).hasPrincipal() == true){ //aquele status � um principal
				if( (this.status.get(i).getPrincipal() == 1) && (this.hasprincipal1==false)){//antes de inserir, verificar
					this.hasprincipal1 = true;
					this.principal1 = this.status.get(i).getStatusId();
				}
				else{
					if( (this.status.get(i).getPrincipal() == 2) && (this.hasprincipal2==false)){//antes de inserir, verificar
						this.hasprincipal2 = true;
						this.principal2 = this.status.get(i).getStatusId();
					}
					else{
						//erro: mais de 1 marcado como principal!
					}
				}
			}
		}
	}
	
	public ArrayList<Service> mapJSONArraytoServices(JSONArray arrayservs) throws JSONException{
		ArrayList<Service> temp_services = new ArrayList<Service>(); 
		
		Log.i("APPLIANCE", "Entrei em mapJSONArraytoServices");
		
		int n = arrayservs.length();
		Log.i("APPLIANCE", "n = " + Integer.toString(n));
		
		// Percorre o JSONArray instanciando Service para cada index
		Log.i("APPLIANCE","Salvar os servi�os");
		Log.i("APPLIANCE","Vou entrar no for");
		for(int i = 0; i < n; i++){
			
			// Cria um JSONOBject temporário
			Log.i("APPLIANCE","Criando Service["+i+"]");
			JSONObject jObj = arrayservs.getJSONObject(i);
			Service serv = new Service(jObj);
			temp_services.add(serv);
		}
		return temp_services;
	}
	
	public ArrayList<Status> mapJSONArraytoStatus(JSONArray arraystatus) throws JSONException{
		ArrayList<Status> temp_status = new ArrayList<Status>();
		
		Log.i("APPLIANCE", "Entrei em mapJSONArraytoStatus");
		
		int n = arraystatus.length();
		Log.i("APPLIANCE", "n = " + Integer.toString(n));
		
		// Percorre o JSONArray instanciando Status para cada index
		Log.i("APPLIANCE","Salvar os status");
		Log.i("APPLIANCE","Vou entrar no for");
		for(int i = 0; i < n; i++){
			
			// Cria um JSONOBject temporário
			Log.i("APPLIANCE","Criando Status["+i+"]");
			JSONObject jObj = arraystatus.getJSONObject(i);
			Status stat = new Status(jObj);
			temp_status.add(stat);
		}
		return temp_status;
	}
	
	public boolean hasPrincipal1(){
		return this.hasprincipal1;
	}
	
	public boolean hasPrincipal2(){
		return this.hasprincipal2;
	}
	
	public boolean hasPrincipal(){
		boolean b = (this.hasprincipal1 || this.hasprincipal2);
		return b;
	}

	public String getIdStatusPrincipal1(){
		return principal1;
	}
	
	public String getIdStatusPrincipal2(){
		return principal2;
	}
	
	public Status getStatusPrincipal1(){ //se ja tem principal1 e quer obter o Status completo
		Status s = null;
		for(int i = 0; i < this.status.size(); i++){
			if ((this.status.get(i).getStatusId()).equals(this.principal1)){
				s = this.status.get(i);
			}	
		}
		// if(s == null) principal1 n�o existe
		return s;
	}
	
	public Status getStatusPrincipal2(){ //se ja tem principal2 e quer obter o Status completo
		Status s = null;
		for(int i = 0; i < this.status.size(); i++){
			if ((this.status.get(i).getStatusId()).equals(this.principal2)){
				s = this.status.get(i);
			}	
		}
		// if(s == null) principal2 n�o existe
		return s;
	}
	
	

	public String getId(){
		return this.id;
	}
	
	public String getKind(){
		return this.kind;
	}
	
	public String getType(){
		return this.type;
	}
	
	public String getVisualId(){
		return this.visual_id;
	}
	
	public String getVisualName(){
		return this.visual_name;
	}
	
	public Bitmap getVisualImage(){
		return this.visual_image;
	}
	
	public Service getService(int i){
		return this.services.get(i);
	}
	
	public Status getStatus(int i){
		return this.status.get(i);
	}
	
	public Service getServiceById(String x){
		Service s = null;
		for(int i = 0; i < this.services.size(); i++){
			if ((this.services.get(i).getServId()).equals(x)){
				s = this.services.get(i);
				break;
			}	
		}
		return s;
	}
	
	public Status getStatusById(String x){
		Status s = null;
		for(int i = 0; i < this.status.size(); i++){
			if ((this.status.get(i).getStatusId()).equals(x)){
				s = this.status.get(i);
				break;
			}	
		}
		return s;
	}
	
	public ArrayList<Service> getAllServices(){
		return this.services;
	}
	
	public ArrayList<Status> getAllStatus(){
		return this.status;
	}
	
	
}

//---subclasses---
/*
 * Classes used on the construction of each object in the house. The JSONObjects
 * and JSONArrays are mapped in subclasses with their own access methods.
 *  
 */







class Service {
	// atributos
	private String id;
	private String name;
	private String image;
	private ArrayList<String> input_id = new ArrayList<String>();
	private ArrayList<String> input_type = new ArrayList<String>();
	private ArrayList<String> status_id = new ArrayList<String>();
	//campo opcional
	private boolean hasnames;
	private boolean hasreturntype;
	private ArrayList<String> names = new ArrayList<String>();
	private ArrayList<String> return_type = new ArrayList<String>();
	
	// construtor
	public Service(JSONObject jObj) throws JSONException{
		
		Log.i("SERVICE","Entrei no construtor de Service");
		
		// Mapeia o JSONObject de index 'i' em um service
		this.id = jObj.getString("id");
		this.name = jObj.getString("name");
		String image_url = jObj.getString("image");
		this.image = image_url.substring( image_url.lastIndexOf('/')+1, image_url.length() );
		
		JSONArray inpid_array = jObj.getJSONArray("input_id");
		JSONArray inptype_array = jObj.getJSONArray("input_type");
		JSONArray stid_array = jObj.getJSONArray("status_id");
		
		//Preencher o ArrayList<String> com o String[] do JSONObject de INPUT_ID, INPUT_TYPE e STATUS_ID
		for(int i = 0; i < inpid_array.length(); i++){
			this.input_id.add(inpid_array.getString(i));
			this.input_type.add(inptype_array.getString(i));	
		}
		
		for(int i = 0; i < stid_array.length(); i++){
			this.status_id.add(stid_array.getString(i));	
		}
		
		//Se esses String[] tiverem tamanho > 1, tamb�m ter� o campo NAMES
		//Entao preencher tamb�m
		if (jObj.has("names")){
			this.hasnames = true;
			JSONArray names_array = jObj.getJSONArray("names");
			for(int i = 0; i < names_array.length(); i++){
				this.names.add(names_array.getString(i));
			}
		} else{
			this.hasnames = false;
		}
		
		//Se esse JSONObject tiver o campo RETURN_TYPE
		//Entao preencher tamb�m
		if (jObj.has("return_type")){
			this.hasreturntype = true;
			JSONArray rtype_array = jObj.getJSONArray("return_type");
			for(int i = 0; i < rtype_array.length(); i++){
				this.return_type.add(rtype_array.getString(i));
			}
		} else {
			this.hasreturntype = false;
		}
	}

	public String getServId() {
		return this.id;
	}
	
	public String getServName() {
		return this.name;
	}
	
	public String getServImage() {
		return this.image;
	}
	
	public ArrayList<String> getInput_id(){
		return this.input_id;
	}
	
	public ArrayList<String> getInput_type(){
		return this.input_type;
	}
	
	public ArrayList<String> getStatus_id(){
		Log.i("DEBUGANDOOOOO","Funcao getStatus_id()");
		for(int i=0; i< this.status_id.size(); i++){
			Log.i("getStatus_id()",this.status_id.get(i));
		}
		return this.status_id;
	}
	
	public boolean hasNames() {
		return this.hasnames;
	}

	public boolean hasReturn_type() {
		return this.hasreturntype;
	}
	
	public ArrayList<String> getNames(){
		return this.names;
	}
	
	public ArrayList<String> getReturn_type(){
		return this.return_type;
	}
	
	
	
}







class Status {
	// atributos
	private String id;
	private String type;
	//campos correspondentes a VALUE
	private boolean value1;
	private int value2;
	private String value3;
	private ArrayList<Integer> value4 = new ArrayList<Integer>();

	//campos opcionais
	private boolean hasprincipal;
	private int principal;
	private String name_false;
	private String name_true;
	private int range1;
	private int range2;
	private ArrayList<String> options = new ArrayList<String>();
	
	// construtor
	public Status(JSONObject jObj) throws JSONException{
		
		Log.i("STATUS","Entrei no construtor de Status");
		// Mapeia o JSONObject de index 'i' em um status
		
		//Pega ID e TYPE
		this.id = jObj.getString("id");
		this.type = jObj.getString("type");
		
		//Verifica se tem PRINCIPAL
		if (jObj.has("principal")){
			this.hasprincipal = true;
			this.principal = jObj.getInt("principal");
		}
		else{
			this.hasprincipal = false;
		}
		
		//Pega VALUE de acordo com TYPE
		if(this.type.equals("boolean")){
			//boolean
			this.value1 = jObj.getBoolean("value");
			Log.i("Status","Salvando boolean, value:"+this.value1);
			//name_true e name_false
			this.name_false = jObj.getString("name_false");
			this.name_true = jObj.getString("name_true");
		}
		else {
			if(this.type.equals("int_range_granular") || this.type.equals("int_range_gradual")){
				//int_range
				this.value2 = jObj.getInt("value");
				Log.i("Status","Salvando inteiro, value:"+this.value2);
				//range 1 e range 2
				JSONArray rangearray = jObj.getJSONArray("range");
				this.range1 = rangearray.getInt(0);
				this.range2 = rangearray.getInt(1);
				Log.i("Status","E ainda r1:"+this.range1+", r2:"+this.range2);
			}
			else {
				if(this.type.equals("string")){
					//string
					this.value3 = jObj.getString("value");
					Log.i("Status","Salvando string, value:"+this.value3);
				} else {
					if(this.type.equals("option")){
						//option
						this.value3 = jObj.getString("value");
						Log.i("Status","Salvando option (string), value:"+this.value3);
						//options
						JSONArray optionsarray = jObj.getJSONArray("options");
						Log.i("Status","E ainda, as seguintes opcoes:");
						for(int i = 0; i < optionsarray.length(); i++){ //preencher o ArrayList<String> com as opcoes do String[]
							this.options.add(optionsarray.getString(i));
							Log.i("Status",optionsarray.getString(i));
						}
					}
					else{
						if(this.type.equals("date")){
							//date
							int tdate = jObj.getJSONObject("value").getInt("day");
							this.value4.add(tdate);
							tdate = jObj.getJSONObject("value").getInt("month");
							this.value4.add(tdate);
							tdate = jObj.getJSONObject("value").getInt("year");
							this.value4.add(tdate);
							Log.i("Status","Salvando date:"+this.value4.get(0)+"/"+this.value4.get(1)+"/"+this.value4.get(2));
						}
						else{
							if(this.type.equals("hour")){
								//hour
								int thour = jObj.getJSONObject("value").getInt("hour");
								this.value4.add(thour);
								thour = jObj.getJSONObject("value").getInt("minute");
								this.value4.add(thour);
								Log.i("Status","Salvando hour, value:"+this.value4.get(0)+":"+this.value4.get(1));
							}
						}
					}
				}
			}
		}
		//se nao for nenhum desses, deu algum erro
	}

	

	
	public String getStatusId(){
		return this.id;
	}
	
	public String getStatusType(){
		return this.type;
	}
	
	public boolean getValueBoolean(){
		return this.value1;
	}
	
	public int getValueInt(){
		return this.value2;
	}
	
	public String getValueString(){
		return this.value3;
	}
	
	public ArrayList<Integer> getValueArrayInt(){
		return this.value4;
	}
	
	public boolean hasPrincipal(){
		return this.hasprincipal;
	}
	
	public int getPrincipal(){
		return this.principal;
	}
	
	public String getName_false(){
		return this.name_false;
	}
	
	public String getName_true(){
		return this.name_true;
	}
	
	public int getRange1(){
		return this.range1;
	}
	
	public int getRange2(){
		return this.range2;
	}
	
	public ArrayList<String> getOptions(){
		return this.options;
	}

	
	
}





