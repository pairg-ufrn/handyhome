package pairg.handyhome;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Bitmap;
import android.util.Log;

public class Scene {
	
	private String id;
	private String visual_id;
	private String visual_name;
	private Bitmap visual_image;
	private int nactions;
	//private ArrayList<Action> actions = new ArrayList<Action>();
	
	public Scene(String imgname, String id, String visual_id, String visual_name, Bitmap visual_image, JSONArray jactions) {
		super();
		this.id = id;
		this.visual_id = visual_id;
		this.visual_name = visual_name;
		this.visual_image = visual_image;
		
		Log.i("SCENE", "Construtor! Instanciando a cena de id ="+id);
		
		/*//DEBUG DE SCENE
		Log.i("SCENE", "CENA "+id);
		Log.i("SCENE", "Nome = "+this.visual_name);
		Log.i("SCENE", "Imagem = "+imgname);
		//por enquanto, somente pegando o numero de cenas do JSON
		int n = jactions.length();
		this.nactions = n;
		Log.i("SCENE - mapJSONtoScenes", "nActions = " + Integer.toString(nactions));*/
		
		//this.actions = mapJSONArraytoActions(jactions);
	}
	
	/*//Parte para pegar Actions da cena
	public ArrayList<Action> mapJSONArraytoActions(JSONArray arrayacts) throws JSONException{
		ArrayList<Action> temp_actions = new ArrayList<Action>(); 
		Log.i("SCENE", "Entrei em mapJSONArraytoActions");
		
		int n = arrayacts.length();
		Log.i("SCENE", "n = " + Integer.toString(n));
		// Percorre o JSONArray instanciando Action para cada index
		Log.i("SCENE","Salvar as acoes");
		Log.i("SCENE","Vou entrar no for");
		for(int i = 0; i < n; i++){
			// Cria um JSONOBject temporario
			Log.i("SCENE","Criando Service["+i+"]");
			JSONObject jObj = arrayacts.getJSONObject(i);
			Action a = new Action(jObj);
			temp_actions.add(a);
		}
		return temp_actions;
	}*/
	
	// Getters and Setters
	public String getId() {
		return this.id;
	}

	// Visual Id
	public String getVisualId() {
		return this.visual_id;
	}
	public void setVisualId(String vid){
		this.visual_id = vid;
	}
	
	// Name
	public String getVisualName() {
		return this.visual_name;
	}
	public void setName(String name) {
		this.visual_name = name;
	}
	
	// Image
	public Bitmap getVisualImage() {
		return this.visual_image;
	}
	public void setImage(Bitmap image) {
		this.visual_image = image;
	}
	
	// Number of Actions
	public int getNActions() {
		return this.nactions;
	}
	

	
	
	
	
	
/*class Action {
	//atributos
	private String name;
	private String image;
	
	public Action(JSONObject jObj) throws JSONException{
	//tratar jObj que descreve cada Action e salvar nos atributos da classe
		
	}
	
	//metodos
	public String getActionName() {
		return this.name;
	}
	
	public String getActionImage() {
		return this.image;
	}
}*/
	
}