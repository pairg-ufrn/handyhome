package pairg.handyhome;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.R.color;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Shader.TileMode;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class CopyOfScenesActivity extends Activity implements AdapterView.OnItemClickListener, View.OnClickListener {
    
	private ActionBar actionBar;
	ArrayList<Scene> myScenesArray =  new ArrayList<Scene>();
	
	//VARIAVEIS NavDrawer 2
			String[] mOptions ;
			private DrawerLayout mDrawerLayout;
			private ListView mDrawerList;
			private ActionBarDrawerToggle mDrawerToggle;
			private LinearLayout mDrawer ;
			private List<HashMap<String,String>> mList ;
			private SimpleAdapter mAdapter;
			final private String NAME = "option_name";
			final private String ICON = "option_icon";
			int[] mFlags = new int[]{
					 R.drawable.ic_drawer_profile,
					 R.drawable.ic_drawer_home,
					 R.drawable.ic_drawer_scenes,
					 R.drawable.ic_drawer_favorites,
					 R.drawable.ic_drawer_settings,
					 R.drawable.ic_drawer_about,
					 R.drawable.ic_drawer_exit
			};
			final private int TAM = mFlags.length;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ListView list = new ListView(this);
        list.setBackgroundColor(Color.WHITE);
        setContentView(list);
        
        // Initialization
     	actionBar = getActionBar();
     	actionBar.setDisplayShowTitleEnabled(false);
     	
     	// Enabling Home button
	    actionBar.setHomeButtonEnabled(true);
	    // Enabling Up Navigation
	    actionBar.setDisplayHomeAsUpEnabled(true);
        
	    ArrayList<String> items = new ArrayList();
	    try{
			// Pede o JSONObject
			Log.i("SCENES","Vou pegar o JSON das cenas");
			JSONObject myJSON = HTTPRequest.getScenes();
			
			Log.i("SCENES","Peguei o JSON de HTTPRequest");
			Log.i("SCENES","myScenesArray tam = "+myScenesArray.size());
			Log.i("SCENES","Agora vou para JSONInterpreter");
			
			myScenesArray = JSONInterpreter.mapJSONtoScenes(myJSON);
			
			Log.i("SCENES","Voltei do JSONInterpreter");
			Log.i("LEVEL2","myScenesArray tam: "+myScenesArray.size());
		}catch(JSONException e){
			e.printStackTrace();
		}
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	   /*
	  //Navigation Drawer 2 Init
		Log.i("ABOUT","Iniciou Navigation Drawer");
		try{
			// Pede o JSONObject 
			Log.i("ABOUT","a1");
			JSONObject myUserJSON = HTTPRequest.getUserImageAndName();
			Log.i("ABOUT","a2");
			ArrayList<String> userinfo = JSONInterpreter.mapJSONtoUserBasicInfo(myUserJSON);
			Log.i("ABOUT","a3");
			TextView username = (TextView) findViewById(R.id.usernamedrawer5);
			Log.i("ABOUT","a4");
			username.setText(userinfo.get(0));
			Log.i("ABOUT","a5");
			ImageView userimage = (ImageView) findViewById(R.id.userimagedrawer5);
			Log.i("ABOUT","a6");
			userimage.setImageBitmap(createRoundImage(JSONInterpreter.getImageFromName(userinfo.get(1))));
			Log.i("ABOUT","a7");
		} catch(JSONException e){
			Log.i("ABOUT","Exception!!!");
			e.printStackTrace();
		}
		
		Log.i("ABOUT","P1");
		mOptions = getResources().getStringArray(R.array.drawer_options);
		mDrawerList = (ListView) findViewById(R.id.drawer_list5);
		mDrawer = (LinearLayout) findViewById(R.id.drawer_linearlayout5);
		Log.i("ABOUT","P2");
		mList = new ArrayList<HashMap<String,String>>();
		for(int i=0;i<TAM;i++){
			HashMap<String, String> hm = new HashMap<String,String>();
			hm.put(NAME, mOptions[i]);
			hm.put(ICON, Integer.toString(mFlags[i]) );
			mList.add(hm);
		}
		Log.i("ABOUT","P3");
		/// Keys used in Hashmap
		String[] from = { ICON,NAME };
		//Ids of views in item_layout
		int[] to = { R.id.drawer_option_icon , R.id.drawer_option_name };
		Log.i("ABOUT","P4");
		// Instantiating an adapter to store each items
		mAdapter = new SimpleAdapter(this, mList, R.layout.draweritem, from, to);
		// Getting reference to DrawerLayout
		mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer5);
		// Creating a ToggleButton for NavigationDrawer with drawer event listener
		Log.i("ABOUT","P5");
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer , R.string.drawer_open,R.string.drawer_close){
		 // Called when drawer is closed 
		 public void onDrawerClosed(View view) {
			 //highlightSelectedCountry();
			 invalidateOptionsMenu();
		 }
		// Called when a drawer is opened 
		 public void onDrawerOpened(View drawerView) {
			 //getSupportActionBar().setTitle("Select a Country");
			 invalidateOptionsMenu();
		 }
		 };
		 Log.i("ABOUT","P6");
		 // Setting event listener for the drawer
		 mDrawerLayout.setDrawerListener(mDrawerToggle);
		 // ItemClick event handler for the drawer items
		 Log.i("ABOUT","P7");
		 mDrawerList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			 public void onItemClick(AdapterView<?> arg0, View arg1, int drawerposition,
			 long arg3) {
			 mDrawerList.setItemChecked(drawerposition, false);
			 Log.i("ABOUT","P8");
			 // Closing the drawer
			 mDrawerLayout.closeDrawer(mDrawer);
			// Intent - Calling the correspondent option Activity
             Log.i("ABOUT","Vou configurar o intent de chamada da Activity da opcao");
             	 Intent i;
	             switch (drawerposition) {
	             case 0:  //perfil (atualmente chamando tutorial)
	            	 	  i = new Intent(getApplicationContext(), TutorialActivity.class);
				  		  i.putExtra("position_drawer", drawerposition);
				  		  i.putExtra("option_name", mOptions[drawerposition]);
				  		  startActivity(i);
				  		  break;
	             case 1:  //principal
	            	 	  i = new Intent(getApplicationContext(), MainActivity.class);
	            	 	  i.putExtra("position_drawer", drawerposition);
	            	 	  i.putExtra("option_name", mOptions[drawerposition]);
	            	 	  startActivity(i);
	            	 	  break;
	             case 2:  //cenarios (atualmente chamando tutorial)
	            	 	  i = new Intent(getApplicationContext(), TutorialActivity.class);
	            	 	  i.putExtra("position_drawer", drawerposition);
	            	 	  i.putExtra("option_name", mOptions[drawerposition]);
	            	 	  startActivity(i);
	                      break;
	             case 3:  //cenarios favoritos (atualmente chamando tutorial)
	            	 	  i = new Intent(getApplicationContext(), TutorialActivity.class);
	            	 	  i.putExtra("position_drawer", drawerposition);
	            	 	  i.putExtra("option_name", mOptions[drawerposition]);
	            	 	  startActivity(i);
                     break;
	             case 4:  //configuracoes
	            	 	  i = new Intent(getApplicationContext(), QuickPrefsActivity.class);
	            	 	  i.putExtra("position_drawer", drawerposition);
	            	 	  i.putExtra("option_name", mOptions[drawerposition]);
	            	 	  startActivity(i);
	                      break;
	             case 5:  //SOBRE, NADA
	            	 	  break;
	             case 6:  //desconectar
	            	 	  HTTPRequest.logoff(); //para desconectar com o servidor
	            	 	  i = new Intent(getApplicationContext(), LoginActivity.class);
	            	 	  i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	            	 	  startActivity(i);
	             default: i = new Intent(getApplicationContext(), TutorialActivity.class);
	 					  i.putExtra("position_drawer", drawerposition);
	 					  i.putExtra("option_name", mOptions[drawerposition]);
	 					  startActivity(i);
	                      break;
	             } 
             
	             //fim do switch
				
			 }
		 });
		 Log.i("ABOUT","P9");
		// Enabling Home button
	    actionBar.setHomeButtonEnabled(true);
	    // Enabling Up Navigation
	    actionBar.setDisplayHomeAsUpEnabled(true);
		// Setting the adapter to the listView
		mDrawerList.setAdapter(mAdapter);
		
		//Colocar um Listener na linha de identificacao do usuario para nao fazer nada
		LinearLayout userInfoDrawerRow = (LinearLayout) findViewById(R.id.linearlayout_profile4);
		userInfoDrawerRow.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.i("ABOUT","P10");
				// TODO Auto-generated method stub
			}
		});
		//Navigation Drawer 2 End
	    
	    */
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    //preenchendo os valores do array de string (nomes das cenas)
        for(int i=0; i<myScenesArray.size(); i++){
        	items.add( (myScenesArray.get(i)).getVisualName() );
        }

        //Supply this adapter with R.layout.scenesrow
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.scenesrow, R.id.text, items) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View row =  super.getView(position, convertView, parent);
                ImageView left = (ImageView)row.findViewById(R.id.left);
                left.setImageBitmap( (myScenesArray.get(position)).getVisualImage() );
                //left.setTag(position);
                //left.setOnClickListener(ScenesActivity.this);
                View right = row.findViewById(R.id.right);
                right.setTag(position);
                right.setOnClickListener(CopyOfScenesActivity.this);
                
                return row;
            }
        };
        
        list.setAdapter(adapter);
        list.setOnItemClickListener(this);
    }
    
    @Override
    public void onClick(View v) {
        switch(v.getId()) {
        case R.id.left:
            Toast.makeText(this, "Left Accessory "+v.getTag(), Toast.LENGTH_SHORT).show();
            break;
        case R.id.right:
            Toast.makeText(this, "Right Accessory "+v.getTag(), Toast.LENGTH_SHORT).show();
            break;
        default:
            break;
        }
    }
    
    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        Toast.makeText(this, "Item Click "+position, Toast.LENGTH_SHORT).show();
    }

    //MYMYMY
    
    /*
    
	@Override
	// Add items to the ActionBar if it is presented
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		//MenuInflater inflater = getMenuInflater();
		//inflater.inflate(R.menu.login, menu);
		Log.i("ABOUT","P11");
		return super.onCreateOptionsMenu(menu);
	}
	
	//methods
	@Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        Log.i("ABOUT","P12");
        mDrawerToggle.syncState();
    }
	
	//More Methods related to Navigation Drawer
	// Handling the touch event of app icon
    @Override
    public boolean onOptionsItemSelected(MenuItem item) { 
        if (mDrawerToggle.onOptionsItemSelected(item)) {
        	Log.i("ABOUT","P13");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
 // Related to Navigation Drawer 2
    private Bitmap createRoundImage(Bitmap img){
		Bitmap circleBitmap = Bitmap.createBitmap(img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);
	    BitmapShader shader = new BitmapShader(img, TileMode.CLAMP, TileMode.CLAMP);
	    Paint paint = new Paint();
	    paint.setAntiAlias(true);
	    paint.setShader(shader);
	    Log.i("ABOUT","P14");
	    Canvas c = new Canvas(circleBitmap);
	    c.drawCircle(img.getWidth() / 2, img.getHeight() / 2, img.getWidth() / 2, paint);
	    return circleBitmap;
	}*/

    




}
