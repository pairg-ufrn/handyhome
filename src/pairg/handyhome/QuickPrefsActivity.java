package pairg.handyhome;

import android.content.Intent;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.Preference.OnPreferenceChangeListener;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

public class QuickPrefsActivity extends PreferenceActivity {
	@Override
	    public void onCreate(Bundle savedInstanceState) {       
	        super.onCreate(savedInstanceState);       
	        addPreferencesFromResource(R.xml.preferences); 
	        
	        CheckBoxPreference pref_vibrate = (CheckBoxPreference) findPreference("feedback_vibrate");
	        
	        if(pref_vibrate.isChecked()){
	        	MyHandyHome.setFVibrateActive();
	        }
			else{
				MyHandyHome.setFVibrateInactive();
			}
	        
	        pref_vibrate.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
				@Override
				public boolean onPreferenceChange(Preference preference,Object newValue) {
					boolean vibrate_checked = Boolean.valueOf(newValue.toString());
					if(vibrate_checked){
						MyHandyHome.setFVibrateActive();
					}
					else{
						MyHandyHome.setFVibrateInactive();
					}
					
	                return true;
				}
	        });
	        
	        CheckBoxPreference pref_audio = (CheckBoxPreference) findPreference("feedback_audio");
	        
	        if(pref_audio.isChecked()){
	        	MyHandyHome.setFAudioActive();
	        }
			else{
				MyHandyHome.setFAudioInactive();
			}
	        
	        pref_audio.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
				@Override
				public boolean onPreferenceChange(Preference preference,Object newValue) {
					boolean audio_checked = Boolean.valueOf(newValue.toString());
					if(audio_checked){
						MyHandyHome.setFAudioActive();
					}
					else{
						MyHandyHome.setFAudioInactive();
					}
					
	                return true;
				}
	        });
	        
	        CheckBoxPreference pref_voice = (CheckBoxPreference) findPreference("voice_interaction");
	        pref_voice.setEnabled(false);
	        
	        CheckBoxPreference pref_gesture = (CheckBoxPreference) findPreference("gesture_interaction");
	        pref_gesture.setEnabled(false);
	    }
	
	
		/*
	    @Override
	    public boolean onCreateOptionsMenu(Menu menu) {
	    	Log.i("balb", "Chegou aqui");
	        menu.add(Menu.NONE, 0, 0, "Show current settings");
	        return super.onCreateOptionsMenu(menu);
	    }
	    
	    @Override
	    public boolean onOptionsItemSelected(MenuItem item) {
	    	return false;
        }
        */
}
