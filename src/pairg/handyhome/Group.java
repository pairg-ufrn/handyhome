package pairg.handyhome;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Bitmap;
import android.util.Log;

public class Group {
	
	private String id;
	private String name;
	private Bitmap image;
	private int objects;
	
	
	public Group(String id, Bitmap image, String name, int objects) {
		super();
		this.id = id;
		this.image = image;
		this.name = name;
		this.objects = objects;
	}
	
	// Getters and Setters
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	// Name
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	// Image
	public Bitmap getImage() {
		return this.image;
	}
	public void setImage(Bitmap image) {
		this.image = image;
	}
	
	// Objects
	public int getObjects() {
		return objects;
	}
	public void setObjects(int objects) {
		this.objects = objects;
	}
	
	
}