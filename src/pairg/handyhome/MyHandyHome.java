package pairg.handyhome;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.CookieStore;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class MyHandyHome {
	// Minhas variáveis de classe
	private static String TAG = MyHandyHome.class.getName();

	private static boolean feedback_vibrate = true;
	private static boolean feedback_audio = false;
	private static boolean interaction_voice = false;
	private static boolean interaction_gesture = false;
	private static int x = 10;
	
	
	
	
	//VIBRATE FEEDBACK
	public static boolean getFVibrate() {
	//return true (for vibration active) or false (for vibration inactive)
		return feedback_vibrate;
	}
	
	public static void setFVibrateActive(){
	//if this method is called, set vibrate active (vibrate = true)
		feedback_vibrate = true;
	}
	
	public static void setFVibrateInactive(){
	//if this method is called, set vibrate inactive (vibrate = false)
		feedback_vibrate = false;
	}
	
	//AUDIO FEEDBACK
	public static boolean getFAudio() {
		//return true (for audio active) or false (for audio inactive)
			return feedback_audio;
		}
		
	public static void setFAudioActive(){
	//if this method is called, set audio feedback active (audio = true)
		feedback_audio = true;
	}
	
	public static void setFAudioInactive(){
	//if this method is called, set audio feedback inactive (audio = false)
		feedback_audio = false;
	}
	
	//VOICE INTERACTION
	public static boolean getIVoice() {
	//return true (for voice interaction active) or false (for voice interaction inactive)
		return interaction_voice;
	}
	
	//GESTURE INTERACTION
	public static boolean getIGesture() {
	//return true (for gesture interaction active) or false (for gesture interaction inactive)
		return interaction_gesture;
	}
	
	
}
