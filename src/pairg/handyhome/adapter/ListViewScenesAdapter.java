package pairg.handyhome.adapter;

import pairg.handyhome.MyHandyHome;
import pairg.handyhome.R;
import pairg.handyhome.Scene;
import pairg.handyhome.ScenesActivity;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.util.Log;
import android.util.StateSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ListViewScenesAdapter extends ArrayAdapter<Scene> {

	Context context;
	int layoutResourceId;
	ArrayList<Scene> data = new ArrayList<Scene>();

	public ListViewScenesAdapter(Context context, int layoutResourceId, ArrayList<Scene> data) {
		super(context, layoutResourceId, data);
		Log.i("ADAPTER DE CENAS","ENTROU NO CONSTRUTOR!!!");
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
	}
	

	//Supply this adapter with R.layout.scenesrow
    //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.scenesrow, R.id.text, items) {};
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
    	Log.i("ADAPTER DE CENAS","ENTROU NO getView()!!!");
        final String nomedacena;
    	
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View row = inflater.inflate(layoutResourceId, parent, false);
        
        ImageView left = (ImageView)row.findViewById(R.id.left);
        left.setImageBitmap( (data.get(position)).getVisualImage() );
        TextView middle = (TextView)row.findViewById(R.id.text);
        middle.setText( (data.get(position)).getVisualName() );
        nomedacena = (data.get(position)).getVisualName();
        View right = row.findViewById(R.id.right);
        right.setTag(position);
        
        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context,"You clicked image " + position, Toast.LENGTH_LONG).show();
                Toast.makeText(context,"Cena  \"" + nomedacena + "\"  executada!", Toast.LENGTH_SHORT).show();
                if(MyHandyHome.getFAudio()){
	                try {
	                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
	                    Ringtone r = RingtoneManager.getRingtone(context, notification);
	                    r.play();
	                } catch (Exception e) {}
                }
                if(MyHandyHome.getFVibrate()){
					Vibrator vib = (Vibrator) context.getSystemService(context.VIBRATOR_SERVICE);
					vib.vibrate(500);
				}
                
            }
        });

        return row;
    }


	/*@SuppressLint("NewApi")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		RecordHolder holder = null;

		if (row == null) {
			//LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(layoutResourceId, parent, false);

			holder = new RecordHolder();
			holder.txtTitle = (TextView) row.findViewById(R.id.item_text);
			holder.imageItem = (ImageView) row.findViewById(R.id.item_image);
			holder.txtObject = (TextView) row.findViewById(R.id.object_text);
			row.setTag(holder);
		} else {
			holder = (RecordHolder) row.getTag();
		}

		Group item = data.get(position);
		holder.txtTitle.setText(item.getName());
		int obj = item.getObjects();
		holder.txtObject.setText(Integer.toString(obj)+" objetos");
		holder.imageItem.setBackgroundColor(context.getResources().getColor(R.color.blue));
		//holder.imageItem.setBackground(createStateListDrawable());
		//holder.imageItem.setImageBitmap(m);
		return row;

	}*/

	
	

	
}