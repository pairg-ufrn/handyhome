package pairg.handyhome.adapter;

import pairg.handyhome.Appliance;
import pairg.handyhome.R;
//import pairg.handyhome.Visual;
import pairg.handyhome.R.color;
import pairg.handyhome.R.id;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class GridViewLevel2Adapter extends ArrayAdapter<Appliance> {

	private ArrayList<OneColor> colors = new ArrayList<OneColor>();
	Context context;
	int layoutResourceId;
	ArrayList<Appliance> data = new ArrayList<Appliance>();

	public GridViewLevel2Adapter(Context context, int layoutResourceId, ArrayList<Appliance> data) {
		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
		activateColors();
	}

	public void activateColors(){
		OneColor c;
		//0 green nethritis
		c = new OneColor(39, 174, 96);
		colors.add(c);
		//1 blue belize hole
		c = new OneColor(41, 128, 185);
		colors.add(c);
		//2 red alizarin
		c = new OneColor(231, 76, 60);
		colors.add(c);
		//3 purple amethyst
		c = new OneColor(155, 89, 182);
		colors.add(c);
		//4 yellow 
		c = new OneColor(204, 156, 0);
		colors.add(c);
		//5 azul petroleo wet asphalt
		c = new OneColor(52, 73, 94);
		colors.add(c);
		//6 orange carrot
		c = new OneColor(230, 126, 34);
		colors.add(c);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		RecordHolder holder = null;

		if (row == null) {
			//LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(layoutResourceId, parent, false);

			holder = new RecordHolder();
			holder.imageItem = (ImageView) row.findViewById(R.id.item_image);
			row.setTag(holder);
		} else {
			holder = (RecordHolder) row.getTag();
		}

		Appliance apitem = data.get(position);
		//Visual v = apitem.getVisual();
		//holder.imageItem.setImageBitmap(apitem.getVisual().getImage());
		holder.imageItem.setImageBitmap(apitem.getVisualImage());
		//holder.imageItem.setBackgroundColor(context.getResources().getColor(R.color.green));
		//holder.imageItem.setBackgroundColor(Color.rgb( colors.get(2).r, colors.get(2).g, colors.get(2).b ));
		int rt = position % 7;
		holder.imageItem.setBackgroundColor(Color.rgb( colors.get(rt).r, colors.get(rt).g, colors.get(rt).b ));
		return row;

	}

	static class RecordHolder {
		ImageView imageItem;

	}
	
	static class OneColor{
		public int r;
		public int g;
		public int b;
		
		public OneColor(int a, int b, int c){
			this.r = a;
			this.g = b;
			this.b = c;
		}
		
	}
	
	
}