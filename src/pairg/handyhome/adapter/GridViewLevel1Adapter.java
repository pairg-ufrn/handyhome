package pairg.handyhome.adapter;

import pairg.handyhome.R;
import pairg.handyhome.Group;
import pairg.handyhome.adapter.GridViewLevel2Adapter.OneColor;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.util.StateSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class GridViewLevel1Adapter extends ArrayAdapter<Group> {

	private ArrayList<OneColor> colors = new ArrayList<OneColor>();
	Context context;
	int layoutResourceId;
	ArrayList<Group> data = new ArrayList<Group>();
	int TAMCOLORS = 0;

	public GridViewLevel1Adapter(Context context, int layoutResourceId,
			ArrayList<Group> data) {
		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
		activateColors();
	}
	
	public void activateColors(){ //blue
		OneColor c;
		TAMCOLORS = 3;
		//0 azul escuro
		c = new OneColor(80, 116, 152);
		colors.add(c);
		//1 azul claro
		c = new OneColor(66, 153, 196);
		colors.add(c);
		//2 cinza 
		c = new OneColor(153, 153, 153);
		colors.add(c);
	}
	
	public void activateColors2(){ //blue
		OneColor c;
		TAMCOLORS = 4;
		//0 azul escuro
		c = new OneColor(80, 116, 152);
		colors.add(c);
		//1 verde aqua
		c = new OneColor(27, 188, 155);
		colors.add(c);
		//4 azul claro
		c = new OneColor(66, 153, 196);
		colors.add(c);
		//3 cinza 
		c = new OneColor(153, 153, 153);
		colors.add(c);
	}
	
	public void activateColors3(){
		OneColor c;
		TAMCOLORS = 7;
		//0 green nethritis
		c = new OneColor(39, 174, 96);
		colors.add(c);
		//1 blue belize hole
		c = new OneColor(41, 128, 185);
		colors.add(c);
		//2 red alizarin
		c = new OneColor(231, 76, 60);
		colors.add(c);
		//3 purple amethyst
		c = new OneColor(155, 89, 182);
		colors.add(c);
		//4 yellow 
		c = new OneColor(204, 156, 0);
		colors.add(c);
		//5 azul petroleo wet asphalt
		c = new OneColor(52, 73, 94);
		colors.add(c);
		//6 orange carrot
		c = new OneColor(230, 126, 34);
		colors.add(c);
	}
	
	private StateListDrawable createStateListDrawable(int rt) {
	    StateListDrawable stateListDrawable = new StateListDrawable();

	    OvalShape ovalShape = new OvalShape();
	    ShapeDrawable shapeDrawable = new ShapeDrawable(ovalShape);
	    //shapeDrawable.getPaint().setColor(context.getResources().getColor(R.color.blue));
	    shapeDrawable.getPaint().setColor(Color.rgb( colors.get(rt).r,colors.get(rt).g,colors.get(rt).b ));
	    stateListDrawable.addState(new int[] { android.R.attr.state_pressed }, shapeDrawable);
	    stateListDrawable.addState(StateSet.WILD_CARD, shapeDrawable);

	    return stateListDrawable;
	}
	
	private Bitmap createRoundImage(Bitmap img){
		Bitmap circleBitmap = Bitmap.createBitmap(img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);
	    BitmapShader shader = new BitmapShader(img, TileMode.CLAMP, TileMode.CLAMP);
	    Paint paint = new Paint();
	    paint.setAntiAlias(true);
	    paint.setShader(shader);
	    Canvas c = new Canvas(circleBitmap);
	    c.drawCircle(img.getWidth() / 2, img.getHeight() / 2, img.getWidth() / 2, paint);
	    return circleBitmap;
	}

	@SuppressLint("NewApi")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		RecordHolder holder = null;

		if (row == null) {
			//LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(layoutResourceId, parent, false);

			holder = new RecordHolder();
			holder.txtTitle = (TextView) row.findViewById(R.id.item_text);
			holder.imageItem = (ImageView) row.findViewById(R.id.item_image);
			holder.txtObject = (TextView) row.findViewById(R.id.object_text);
			row.setTag(holder);
		} else {
			holder = (RecordHolder) row.getTag();
		}

		Group item = data.get(position);
		holder.txtTitle.setText(item.getName());
		int obj = item.getObjects();
		holder.txtObject.setText(Integer.toString(obj)+" objetos");
		holder.imageItem.setBackgroundColor(context.getResources().getColor(R.color.blue));
		int colorpos = position % TAMCOLORS;
		//holder.imageItem.setBackground(createStateListDrawable());
		holder.imageItem.setBackground(createStateListDrawable(colorpos));
		holder.imageItem.setImageBitmap(createRoundImage(item.getImage()));
		//holder.imageItem.setImageBitmap(item.getImage());
		return row;

	}

	
	
	
	static class RecordHolder {
		TextView txtTitle;
		ImageView imageItem;
		TextView txtObject;

	}
	
	static class OneColor{
		public int r;
		public int g;
		public int b;
		
		public OneColor(int a, int b, int c){
			this.r = a;
			this.g = b;
			this.b = c;
		}
		
	}
	
}