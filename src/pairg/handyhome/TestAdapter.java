package pairg.handyhome;

import pairg.handyhome.R;
import pairg.handyhome.Group;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class TestAdapter extends ArrayAdapter<Appliance> {

	Context context;
	int layoutResourceId;
	ArrayList<Appliance> data = new ArrayList<Appliance>();

	public TestAdapter(Context context, int layoutResourceId,
			ArrayList<Appliance> data) {
		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		RecordHolder holder = null;

		if (row == null) {
			//LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(layoutResourceId, parent, false);

			holder = new RecordHolder();
			holder.txtTitle = (TextView) row.findViewById(R.id.item_text);
			holder.imageItem = (ImageView) row.findViewById(R.id.item_image);
			holder.txtObject = (TextView) row.findViewById(R.id.object_text);
			row.setTag(holder);
		} else {
			holder = (RecordHolder) row.getTag();
		}

		Appliance item = data.get(position);
		holder.txtTitle.setText(item.getVisualName());
		holder.txtObject.setText("x objetos");
		holder.imageItem.setImageBitmap(item.getVisualImage());
		holder.imageItem.setBackgroundColor(context.getResources().getColor(R.color.blue));
		return row;

	}

	static class RecordHolder {
		TextView txtTitle;
		ImageView imageItem;
		TextView txtObject;

	}
	
	
}