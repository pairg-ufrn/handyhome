package pairg.handyhome;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import pairg.handyhome.adapter.GridViewLevel2Adapter;
import android.app.ActionBar;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class Level2Activity extends Activity {
	
	private ActionBar actionBar;
	GridView gridView;
	ArrayList<Appliance> gridArray = new ArrayList<Appliance>();
	GridViewLevel2Adapter customGridAdapter;
	String ARG_ID = "id_group";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_level2);
		Log.i("LEVEL2","ESTOU ENTRANDO NO ONCREATE DO LEVEL2!!! \\o/");
		
		//Broadcast Receiver of Logout
	    IntentFilter intentFilter = new IntentFilter();
	    intentFilter.addAction("com.package.ACTION_LOGOUT");
	    registerReceiver(new BroadcastReceiver() {
	        @Override
	        public void onReceive(Context context, Intent intent) {
	            Log.d("onReceive","Logout in progress");
	            //At this point you should start the login activity and finish this one
	            finish();
	        }
	    }, intentFilter);
		
		// Initialization
		actionBar = getActionBar();
		actionBar.setDisplayShowTitleEnabled(false);
		
		// Captura o Id do item que foi clicado
		Bundle extras = getIntent().getExtras();
		String id = extras.getString(ARG_ID);

		try{
			// Pede o JSONObject
			Log.i("LEVEL2","Vou pegar o JSON dos appliances do grupo com id: " + id);
			JSONObject myJSON = HTTPRequest.getObjects(id);
			
			Log.i("LEVEL2","Peguei o JSON de HTTPRequest");
			Log.i("LEVEL2","Antes de ir para JSONInterpreter");
			Log.i("LEVEL2","gridArray tamamho = "+gridArray.size());
			Log.i("LEVEL2","Vou para JSONInterpreter");
			
			gridArray = JSONInterpreter.mapJSONtoAppliances(myJSON);
			Log.i("LEVEL2","Adicionados no gridArray");
			Log.i("LEVEL2","gridArray tamanho: "+gridArray.size());
		}catch(JSONException e){
			e.printStackTrace();
		}
		
		gridView = (GridView) findViewById(R.id.gridViewTestL2);
		customGridAdapter = new GridViewLevel2Adapter(this.getBaseContext(), R.layout.group2, gridArray);
		gridView.setAdapter(customGridAdapter);
		
		gridView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id){
				Toast.makeText(
						getApplicationContext(),
						gridArray.get(position).getVisualName(), 
						Toast.LENGTH_SHORT).show();
				
				/*Intent broadcastIntent = new Intent();
				broadcastIntent.setAction("com.package.ACTION_LOGOUT");
				sendBroadcast(broadcastIntent);
				finish();*/

				
				// Chamar o Level3Activity
				// debug
				Log.i("LEVEL2FRAGMENT","Vou configurar o intent de chamada da Level3Activity");
				Intent i = new Intent(getApplicationContext(), Level3Activity.class);
				Log.i("LEVEL2FRAGMENT","item_text = " + gridArray.get(position).getId());
				
				//Aqui era para usar Serializable ou Parcelable no putExtra, mas como n�o funcionou, vai vari�vel static mesmo (na classe Transfer)!
				//i.putExtra("my_appliance", gridArray.get(position));
				Transfer.setAppliance(gridArray.get(position));
				i.putExtra("position_to_color", position);
				startActivity(i);
				
				
				
				
			}

		});
		
	}

	@Override
	// Add items to the ActionBar if it is presented
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_actions, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	// ActionBar events
    @Override
    public boolean onOptionsItemSelected(MenuItem item) { 
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_favorite:
            	//Toast.makeText(getBaseContext(),"Cenas Favoritas!!! ",Toast.LENGTH_LONG).show();
            	Intent i = new Intent(getApplicationContext(), FavoriteScenesActivity.class);
 	 	  		startActivity(i);
            	return true;
            case R.id.action_refresh:
            	Toast.makeText(getBaseContext(),R.string.refresh_message,Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_voice:
            	Toast.makeText(getBaseContext(),R.string.voiceinteraction_disable,Toast.LENGTH_LONG).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
