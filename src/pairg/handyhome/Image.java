package pairg.handyhome;

import android.graphics.Bitmap;


public class Image {

	// atributos
	private int _id;
	private String _name;
	private byte[] _image;
	
	// construtor
	public Image(int parseInt, String string, byte[] blob) {
		// TODO mexo já com você
	}
	
	public Image() {
		// TODO mexo já com você
	}

	// gets e sets
	public int get_id() {
		return _id;
	}

	public void set_id(int _id) {
		this._id = _id;
	}

	public String get_name() {
		return _name;
	}

	public void set_name(String _name) {
		this._name = _name;
	}

	public byte[] get_image() {
		return _image;
	}

	public void set_image(byte[] _image) {
		this._image = _image;
	}
	
	
}
