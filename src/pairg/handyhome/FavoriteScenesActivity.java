package pairg.handyhome;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import pairg.handyhome.adapter.ListViewFavoriteScenesAdapter;
import pairg.handyhome.adapter.ListViewScenesAdapter;
import android.R.color;
import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Shader.TileMode;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class FavoriteScenesActivity extends Activity  {
    
	private ActionBar actionBar;
	ListView listView;
	ArrayList<Scene> myScenesArray =  new ArrayList<Scene>();
	ListViewFavoriteScenesAdapter customListAdapter;
	
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        /*ListView list = new ListView(this);
        list.setBackgroundColor(Color.WHITE);
        setContentView(list);*/
        setContentView(R.layout.activity_favoritescenes);
        
        // Initialization
     	actionBar = getActionBar();
     	//actionBar.setDisplayShowTitleEnabled(false);
     	getActionBar().setTitle("Atalho - Cen�rios Favoritos");
     	
     	/*// Enabling Home button
	    actionBar.setHomeButtonEnabled(true);
	    // Enabling Up Navigation
	    actionBar.setDisplayHomeAsUpEnabled(true);*/
        
	    ArrayList<String> items = new ArrayList();
	    try{
			// Pede o JSONObject
			Log.i("SCENES","Vou pegar o JSON das cenas");
			JSONObject myJSON = HTTPRequest.getFavoriteScenes();
			
			Log.i("SCENES","Peguei o JSON de HTTPRequest");
			Log.i("SCENES","myScenesArray tam = "+myScenesArray.size());
			Log.i("SCENES","Agora vou para JSONInterpreter");
			
			myScenesArray = JSONInterpreter.mapJSONtoScenes(myJSON);
			
			Log.i("SCENES","Voltei do JSONInterpreter");
			Log.i("LEVEL2","myScenesArray tam: "+myScenesArray.size());
		}catch(JSONException e){
			e.printStackTrace();
		}

	  
		// Enabling Home button
	    actionBar.setHomeButtonEnabled(true);
	    // Enabling Up Navigation
	    actionBar.setDisplayHomeAsUpEnabled(true);
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    //preenchendo os valores do array de string (nomes das cenas)
        for(int i=0; i<myScenesArray.size(); i++){
        	items.add( (myScenesArray.get(i)).getVisualName() );
        }


        Log.i("SCENES","P20");
        listView = (ListView) findViewById(R.id.favoritesceneslistview);
        customListAdapter = new ListViewFavoriteScenesAdapter(this.getBaseContext(), R.layout.scenesfavoritesrow, myScenesArray);
        listView.setAdapter(customListAdapter);
        
        
        
        /*for(int pos=0; pos<myScenesArray.size(); pos++){
        	View myrow = listView.getView(pos);
        }*/
        
        
        listView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v, int position, long id){
				Toast.makeText(getApplicationContext(),"As a��es dessa cena n�o podem ser detalhadas", Toast.LENGTH_SHORT).show();
				// Vibrate for 500 milliseconds
					//Vibrator vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
					//vib.vibrate(500);
				/*
				AlertDialog.Builder mensagem = new AlertDialog.Builder(ScenesActivity.this);
				mensagem.setTitle("Cena "+myScenesArray.get(position).getVisualName());
				mensagem.setMessage("Cena possui as seguintes a��es: X, Y, Z");
				//mensagem.setNeutralButton("OK",null);
				mensagem.setPositiveButton("Executar", new DialogInterface.OnClickListener() {
			           public void onClick(DialogInterface dialog, int id) {
			               // User clicked OK button
			        	   Toast.makeText(getBaseContext(),"Cena  \"" + myScenesArray.get(position).getVisualName() + "\"  executada!", Toast.LENGTH_SHORT).show();
			                if(MyHandyHome.getFVibrate()){
								Vibrator vib = (Vibrator) getSystemService(VIBRATOR_SERVICE);
								vib.vibrate(500);
							}
			           }
			       });
				mensagem.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
			           public void onClick(DialogInterface dialog, int id) {
			               // User cancelled the dialog
			           }
			       });
				AlertDialog dialog = mensagem.create();
				mensagem.show();*/
				// Chamar o OutraActivity
				// debug
				/*Log.i("LEVEL2FRAGMENT","Vou configurar o intent de chamada da Level3Activity");
				Intent i = new Intent(getApplicationContext(), Level3Activity.class);
				Log.i("LEVEL2FRAGMENT","item_text = " + myScenesArray.get(position).getId());
				//Aqui era para usar Serializable ou Parcelable no putExtra, mas como n�o funcionou, vai vari�vel static mesmo (na classe Transfer)!
				//i.putExtra("my_appliance", gridArray.get(position));
				i.putExtra("position_to_color", position);
				startActivity(i);*/
			}
		});
 
        
        
        
    }
    
    /*@Override
    public void onClick(View v) {
        switch(v.getId()) {
        case R.id.left:
            Toast.makeText(this, "Left Accessory "+v.getTag(), Toast.LENGTH_SHORT).show();
            break;
        case R.id.right:
            Toast.makeText(this, "Right Accessory "+v.getTag(), Toast.LENGTH_SHORT).show();
            break;
        default:
            break;
        }
    }
    
    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        Toast.makeText(this, "Item Click "+position, Toast.LENGTH_SHORT).show();
    }*/

    //MYMYMY
    
    
    
	@Override
	// Add items to the ActionBar if it is presented
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		//MenuInflater inflater = getMenuInflater();
		//inflater.inflate(R.menu.login, menu);
		Log.i("SCENES","P11");
		return super.onCreateOptionsMenu(menu);
	}


    




}
