package pairg.handyhome;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

public class TutorialActivity extends FragmentActivity {
	SectionsPagerAdapter mSections;
	
	ViewPager mViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tutorial);
		
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		
		mSections = new SectionsPagerAdapter(
				getSupportFragmentManager());
		
		mViewPager = (ViewPager) findViewById(R.id.pagerTutorial);
		mViewPager.setAdapter(mSections);
		
		mViewPager
		.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				// Atualiza progresso
			}
		});
		
		
	}
	
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a TutorialSectionFragment (defined as a static inner class
			// below) with the page number as its lone argument.
			Fragment fragment = new TutorialSectionFragment();
			Bundle args = new Bundle();
			args.putInt(TutorialSectionFragment.ARG_SECTION_NUMBER, position + 1);
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public int getCount() {
			// Show 7 total pages.
			return 7;
		}

	}
	
	public static class TutorialSectionFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		public static final String ARG_SECTION_NUMBER = "section_number";

		public TutorialSectionFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			
			int position = getArguments().getInt(ARG_SECTION_NUMBER);
			
			View rootView = inflater.inflate(R.layout.fragment_tutorial,
					container, false);
			ImageView sectionImage = (ImageView) rootView
					.findViewById(R.id.imageViewTutorial);
			switch(position){
				case 1:
					//sectionImage.setBackgroundResource(R.drawable.ic_menu_others);

					sectionImage.setImageResource(R.drawable.tutorial1);
					break;
				case 2:
					//sectionImage.setBackgroundResource(R.drawable.ic_menu_others);

					sectionImage.setImageResource(R.drawable.tutorial2);
					break;
				case 3:
					//sectionImage.setBackgroundResource(R.drawable.ic_menu_others);

					sectionImage.setImageResource(R.drawable.tutorial3);
					break;
				case 4:
					//sectionImage.setBackgroundResource(R.drawable.ic_menu_others);

					sectionImage.setImageResource(R.drawable.tutorial4);
					break;
				case 5:
					//sectionImage.setBackgroundResource(R.drawable.ic_menu_others);

					sectionImage.setImageResource(R.drawable.tutorial5);
					break;
				case 6:
					//sectionImage.setBackgroundResource(R.drawable.ic_menu_others);

					sectionImage.setImageResource(R.drawable.tutorial6);
					break;
				case 7:
					//sectionImage.setBackgroundResource(R.drawable.ic_menu_others);

					sectionImage.setImageResource(R.drawable.tutorial7);
					rootView.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Intent myIntent = new Intent(getActivity().getBaseContext(), MainActivity.class);
				        	//myIntent.putExtra(phpsessid, value);
				        	startActivity(myIntent);
						}
					});
					break;
			}
			return rootView;
		}
	}
	
}
