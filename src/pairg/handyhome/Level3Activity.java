package pairg.handyhome;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Level3Activity extends ListActivity{
	
	private ActionBar actionBar;
	//String ARG_APP = "my_appliance"; //N�o vou usar mais porque n�o estou usando getExtra
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_level3);
		Log.i("LEVEL3","ESTOU ENTRANDO NO ONCREATE DO LEVEL3!!! \\o/");
		
		// Initialization
		actionBar = getActionBar();
		actionBar.setDisplayShowTitleEnabled(false);
		
		//Broadcast Receiver of Logout
	    IntentFilter intentFilter = new IntentFilter();
	    intentFilter.addAction("com.package.ACTION_LOGOUT");
	    registerReceiver(new BroadcastReceiver() {
	        @Override
	        public void onReceive(Context context, Intent intent) {
	            Log.d("onReceive","Logout in progress");
	            //At this point you should start the login activity and finish this one
	            finish();
	        }
	    }, intentFilter);
	    
		
		/* Teste de Broadcast de Logout no Level3
		 * Adicionar em activity_level3.xml:
		 * <Button
		 *      android:id="@+id/logoutL3Button"
		 *      android:layout_width="wrap_content"
		 *      android:layout_height="wrap_content"
		 *      android:text="logout" />
		 * E o descomentar o seguinte trecho de codigo:*/
		/*Button blogout = (Button) findViewById(R.id.logoutL3Button);
		blogout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					Intent broadcastIntent = new Intent();
					broadcastIntent.setAction("com.package.ACTION_LOGOUT");
					sendBroadcast(broadcastIntent);
					finish();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});	*/
	    /* Button blogout = (Button) findViewById(R.id.logoutL3Button);
		blogout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					//Intent broadcastIntent = new Intent();
					//broadcastIntent.setAction("com.package.ACTION_LOGOUT");
					//sendBroadcast(broadcastIntent);
					//finish();
					
					//Intent intent = new Intent(ExitConfirmationActivity.this, FirstActivity.class);
			        //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			        //startActivity(intent);
					Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
			        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			        startActivity(intent);
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});	*/
		
		// Captura o appliance do item que foi clicado
		//Aqui era para usar Serializable ou Parcelable no getExtra, mas como n�o funcionou, vai vari�vel static mesmo (na classe Transfer)! OBS: Depois que pegar, n�o esquecer de limpar!
		//Intent it = getIntent(); Appliance my_ap = (Appliance)it.getSerializableExtra(ARG_APP);
		Appliance my_ap = Transfer.getAppliance();
		Transfer.clearAppliance();
		   
		//pegando a posicao para saber a cor
		Bundle extras = getIntent().getExtras();
		int pos = extras.getInt("position_to_color");
		int rt = pos % 7;
		int r=255; int g=255; int b=255;
		switch(rt){
		case 0: //0 green nephritis
			r=39; g=174; b=96;
			break;
		case 1: //1 blue belize hole
			r=41; g=128; b=185;
			break;
		case 2: //2 red alizarin
			r=231; g=76; b=60;
			break;
		case 3: //3 purple amethyst
			r=155; g=89; b=182;
			break;
		case 4: //4 yellow 
			r=204; g=156; b=0;
			break;
		case 5: //5 azul petroleo wet asphalt
			r=52; g=73; b=94;
			break;
		case 6: //6 orange carrot
			r=230; g=126; b=34;
			break;
		}
		
		ImageView my_ap_image = (ImageView) findViewById(R.id.applianceImage);
		my_ap_image.setImageBitmap(my_ap.getVisualImage());
		my_ap_image.setBackgroundColor(Color.rgb( r,g,b ));
		TextView my_ap_name = (TextView) findViewById(R.id.applianceName);
		my_ap_name.setText(my_ap.getVisualName());
		
			// use your own layout
		

			Level3Adapter mAdapter = new Level3Adapter(this, my_ap.getAllServices(), my_ap, pos);
			
			
			ArrayList<Service> svs = my_ap.getAllServices();
			for(int i = 0; i < svs.size(); i++){

				int x = Oraculo.getInterface(my_ap,i);
				Log.i("TEST ACTIVITY","Service ["+i+"] = "+x);
			}

			setListAdapter(mAdapter);


	}
	
	@Override
	// Add items to the ActionBar if it is presented
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_actions, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	// ActionBar events
    @Override
    public boolean onOptionsItemSelected(MenuItem item) { 
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_favorite:
            	//Toast.makeText(getBaseContext(),"Cenas Favoritas!!! ",Toast.LENGTH_LONG).show();
            	Intent i = new Intent(getApplicationContext(), FavoriteScenesActivity.class);
 	 	  		startActivity(i);
            	return true;
            case R.id.action_refresh:
            	Toast.makeText(getBaseContext(),R.string.refresh_message,Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_voice:
            	Toast.makeText(getBaseContext(),R.string.voiceinteraction_disable,Toast.LENGTH_LONG).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
	
}
