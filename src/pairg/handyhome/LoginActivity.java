package pairg.handyhome;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity {
	// variáveis de controle---
	private static String TAG = LoginActivity.class.getName();
		
	// variáveis de conteúdo---
	EditText user, pass;
	Button enter;
	CheckBox session;
	
	// shared preferences---
	private SharedPreferences prefs;
	private String prefName = "MyPref";
	private static final String USER_KEY = "user";
	private static final String PASS_KEY = "pass";
	private static final String FLAG_KEY = "flag";
	
	// shared preferences of firstTime
	private SharedPreferences prefsFT;
	private String prefsFTName = "FTPrefs";
	private static final String USER_FT_KEY = "user";
	private static final String USERS_FT_KEY = "myusersstring";
	int firsttime;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		// define as variaveis de conteudo
		user = (EditText) findViewById(R.id.userEditText);
		pass = (EditText) findViewById(R.id.passEditText);
		enter = (Button) findViewById(R.id.enterButton);
		session = (CheckBox) findViewById(R.id.sessionCheckBox);
		
		/*// SharedPreferences
		// load the SharedPreferences object
		SharedPreferences prefs = getSharedPreferences(prefName, MODE_PRIVATE);
		final SharedPreferences.Editor editor = prefs.edit();
		*/
		// first time to run the application
		prefsFT = getSharedPreferences(prefsFTName, MODE_PRIVATE);
		final SharedPreferences.Editor editorFT = prefsFT.edit();
		
		/*
		Log.i(TAG, "Getting Flag: ");
		boolean flag = prefs.getBoolean(FLAG_KEY, false);//Boolean.parseBoolean(prefs.getString(FLAG_KEY, false));
		Log.i(TAG, "Flag: " + Boolean.toString(flag));
		if(flag == true){
			user.setText(prefs.getString(USER_KEY, ""));
			pass.setText(prefs.getString(PASS_KEY, ""));
			session.setChecked(true);
			
			enter.callOnClick();
		}
		*/
		
		// listener de eventos do botao enter---
		enter.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					Log.i(TAG, "onClick()");
					//Log.i("TESTE SHAREDPREFERENCES",user.getText().toString());
					
					/*if(prefsFT.contains(USER_FT_KEY)){
						Log.i("X", "cont�m USER_FT_KEY!!! Ent�o algu�m j� se logou.");
						// não faça nada, o usuário já viu o modo tutorial
						Log.i("TESTE SHAREDPREFERENCES","firsttime=0");
						firsttime = 0;
					}else{
						// guarda o usuário na lista de quem já viu o modo tutorial
						Log.i("X", "N�o cont�m USER_FT_KEY, vou inserir o usuario");
						editorFT.putString(USER_FT_KEY, user.getText().toString());
						editorFT.commit();
						Log.i("TESTE SHAREDPREFERENCES","firsttime=1");
						firsttime = 1;
						
						// chama o modo tutorial, que é basicamente uma activity
						// com um swipeView de imagens instrutivas	
					}*/
					
					String usuarioatual = user.getText().toString();
					int usuariofound = 0;
					Log.i("X", "USU�RIO ATUAL: "+usuarioatual);
					
					if (prefsFT.contains(USERS_FT_KEY)){ //se USERS_FT_KEY existe, algu�m se logou
						Log.i("X", "cont�m USER_FT_KEY!!! Ent�o algu�m j� se logou.");
						//pegar a string de usu�rios
						Set<String> myStrings = prefsFT.getStringSet(USERS_FT_KEY, new HashSet<String>());
						
						//procurar usuario na string de usuarios
						Log.i("X", "INTERATOR!!! PROCURANDO USUARIOS JA LOGADOS!");
						int x = 1;
						for (Iterator<String> it = myStrings.iterator(); it.hasNext(); ) {
					        String f = it.next();
					        Log.i("X", "usuario["+x+"] = "+f);
					        x++;
					        if (f.equals(usuarioatual))
					            usuariofound = 1;
					    }
						if(usuariofound ==1){//if (se usu�rio estiver na lista)
							Log.i("X", "Algu�m se logou, Esse usu�rio j� se logou");
							//ele j� se logou
							firsttime = 0;
						}
						else{
							Log.i("X", "Algu�m se logou mas, Esse usu�rio n�o se logou.");
							//primeiro login dele, adicionar na lista
							myStrings.add(usuarioatual);
							editorFT.putStringSet(USERS_FT_KEY, myStrings);
							editorFT.commit();
							firsttime = 1;
							//o que foi colocado
							Log.i("X", "Vamos ver o set que foi inserido!!!");
							x = 1;
							for (Iterator<String> it = myStrings.iterator(); it.hasNext(); ) {
						        String g = it.next();
						        Log.i("X", "myStrings["+x+"] = "+g);
						        x++;
						    }
						}
					}
					else{
						Log.i("X", "n�o existe USER_FT_KEY! NINGU�M SE LOGOU AINDA!!!");
						//ningu�m se logou ainda!
						Set<String> myStrings = new HashSet<String>(); //inicializar
						myStrings.add(usuarioatual);
						editorFT.putStringSet(USERS_FT_KEY, myStrings);
						editorFT.commit();
						firsttime = 1;
					}
					
					
					// decide se deve guardar a sessao
					/*Log.i(TAG, "Session: " + Boolean.toString(session.isChecked()));
					if(session.isChecked() == true){
						editor.putBoolean(FLAG_KEY, true);
						editor.putString(USER_KEY, user.getText().toString());
						editor.putString(PASS_KEY, pass.getText().toString());
						editor.commit();
					}*/
					// executa o login em background
					String[] args = {user.getText().toString(), pass.getText().toString()};
					ExecuteLogin login = new ExecuteLogin(getBaseContext());
					Log.i(TAG, "Breaking point: Vai executar o login");
					login.execute(args);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});		
	}
	
	// menu---
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		// TODO Auto-generated method stub
		switch(featureId){
		case 0:
			Intent prefIntent = new Intent(getBaseContext(), QuickPrefsActivity.class);
        	startActivity(prefIntent);
		}
		return super.onMenuItemSelected(featureId, item);
	}



	// classe de AsyncTask para executar tarefa de login em background---
	public class ExecuteLogin extends AsyncTask<String, String, Integer>{
		private ProgressDialog progress;
		private Context context;
		
		// Constructor
		public ExecuteLogin(Context context){
			this.context = context;
		}
		
		@Override
		protected void onPreExecute(){
			// Cria novo ProgressDialog e exibe
			/*Log.i(TAG, "Breaking point: Pré-execução");
			progress = new ProgressDialog(context);
			progress.setMessage("Aguarde...");
			progress.show();*/
		}

		@Override
		protected Integer doInBackground(String... params) {

				Log.i(TAG, "Breaking point: Background");
				Log.i(TAG, "Breaking point: User - " + params[0].toString() + " Pass - " + params[1]);
				Integer aux = HTTPRequest.Login(params[0], params[1]);
				Log.i(TAG, "aux = " + aux.toString());
				return aux;
		}
		
		@Override
		protected void onPostExecute(Integer result){
	        Log.i(TAG, "result = " + result.toString());
	        // Trata o resultado do login
	        if(result == 0){
	        	CharSequence text = "Login e/ou senha inválido";
	        	int duration = Toast.LENGTH_SHORT;
	        	Toast toast = Toast.makeText(this.context, text, duration);
	        	toast.show();
	        }else{
	        	if(firsttime==1){
					AlertDialog.Builder mensagem = new AlertDialog.Builder(LoginActivity.this);
					mensagem.setTitle("Termo de Uso");
					mensagem.setMessage("HandyHome � uma aplica��o cliente do HouseHub, um servi�o de automa��o residencial para controlar e monitorar aparelhos da casa baseado em Arduino. Voc� necessita de um HouseHub instalado em sua casa para utilizar todas as funcionalidades oferecidas por esse app.\n\nA equipe respons�vel pela aplica��o HandyHome n�o se responsabiliza por quaisquer danos em sua resid�ncia, moradores e outros envolvidos, sejam de car�ter acidencial, especial ou consequente em qualquer circunst�ncia. Est�o inclu�dos danos ou perdas materiais, de informa��es, de interrup��o de servi�o ou outras quaisquer caracter�sticas similares.\n\nAo concordar com este termo, voc� permite acesso total �s informa��es disponibilizadas ao sistema pela equipe e desenvolvedores respons�veis pela aplica��o.");
					//mensagem.setNeutralButton("OK",null);
					mensagem.setPositiveButton("Concordo", new DialogInterface.OnClickListener() {
				           public void onClick(DialogInterface dialog, int id) {
				               // User clicked OK button
				        	   Intent myIntent = new Intent(getBaseContext(), TutorialActivity.class);
				        		startActivity(myIntent);
				           }
				       });
					mensagem.setNegativeButton("Discordo", new DialogInterface.OnClickListener() {
				           public void onClick(DialogInterface dialog, int id) {
				               // User cancelled the dialog
				        	   Toast toast = Toast.makeText(context, "Voc� n�o aceitou o termo de uso", Toast.LENGTH_LONG);
				           }
				       });
					AlertDialog dialog = mensagem.create();
					mensagem.show();
	        		
	        	}
	        	else{
	        		Intent myIntent = new Intent(getBaseContext(), MainActivity.class);
	        		startActivity(myIntent);
	        	}
	        	//myIntent.putExtra(phpsessid, value);
	        }
		}
	}
	
}
