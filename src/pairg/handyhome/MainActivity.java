package pairg.handyhome;

/**
 * @author Sarah Sakamoto and Diego Oliveira
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import pairg.handyhome.adapter.GridViewLevel1Adapter;
import pairg.handyhome.adapter.GridViewLevel2Adapter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.StateSet;
import android.view.Menu;
import android.view.MenuInflater;
import android.app.ActionBar;
import android.app.ActionBar.OnNavigationListener;
import android.app.ActionBar.Tab;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnNavigationListener{
	
	private ActionBar actionBar;
	String ARG_SECTION = "section";
	//Handler mHandler;
	
	//VARIAVEIS NavDrawer 2
	String[] mOptions ;
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	private LinearLayout mDrawer ;
	private List<HashMap<String,String>> mList ;
	private SimpleAdapter mAdapter;
	final private String NAME = "option_name";
	final private String ICON = "option_icon";
	int[] mFlags = new int[]{
			 R.drawable.ic_drawer_profile,
			 R.drawable.ic_drawer_home,
			 R.drawable.ic_drawer_scenes,
			 R.drawable.ic_drawer_search,
			 R.drawable.ic_drawer_about,
			 R.drawable.ic_drawer_settings,
			 R.drawable.ic_drawer_exit
	};
	final private int TAM = mFlags.length;
	
	

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_level1);
		
		//Broadcast Receiver of Logout
	    IntentFilter intentFilter = new IntentFilter();
	    intentFilter.addAction("com.package.ACTION_LOGOUT");
	    registerReceiver(new BroadcastReceiver() {

	                    @Override
	                    public void onReceive(Context context, Intent intent) {
	                        Log.d("onReceive","Logout in progress");
	    //At this point you should start the login activity and finish this one
	                        finish();
	                    }
	    }, intentFilter);
		
		
		// Initialization
		actionBar = getActionBar();
		actionBar.setDisplayShowTitleEnabled(false);
		
		Log.i("LEVEL1","Entrando no ONCREATEVIEW da LEVEL1Activity!!! \\o/");
		// Pede o JSONObject
		String section = "home";
		
		//SPINNER
		SpinnerAdapter mSpinnerAdapter = ArrayAdapter.createFromResource(this, R.array.spinner_options,
                android.R.layout.simple_spinner_dropdown_item);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        getActionBar().setListNavigationCallbacks(mSpinnerAdapter,this);
		
		
		//Navigation Drawer 2 Init
		Log.i("LEVEL1","Iniciou Navigation Drawer");
		try{
			// Pede o JSONObject 
			JSONObject myUserJSON = HTTPRequest.getUserBasicInfo();
			Log.i("LEVEL1","a1");
			ArrayList<String> userinfo = JSONInterpreter.mapJSONtoUserBasicInfo(myUserJSON);
			Log.i("LEVEL1","userinfo tam = "+userinfo.size());
			Log.i("LEVEL1","userinfo[0] = "+userinfo.get(0));
			Log.i("LEVEL1","userinfo[1] = "+userinfo.get(1));
			Log.i("LEVEL1","a2");
			TextView username = (TextView) findViewById(R.id.usernamedrawer);
			Log.i("LEVEL1","a3");
			username.setText(userinfo.get(0));
			Log.i("LEVEL1","a4");
			ImageView userimage = (ImageView) findViewById(R.id.userimagedrawer);
			Log.i("LEVEL1","a5");
			Context imgContext = userimage.getContext();
			
			//esses pararam de funcionar
			//nao esta aceitando modificar Bitmap
			//userimage.setImageBitmap(createRoundImagex(JSONInterpreter.getImageFromName(userinfo.get(1))));
			//userimage.setImageBitmap(JSONInterpreter.getImageFromName(userinfo.get(1)));;
			//utilizando setImageResource a partir do ID obtido
			int img_resorce = JSONInterpreter.getImageResIdFromNameAndContext(userinfo.get(1), imgContext);
			userimage.setImageResource(img_resorce);
			Log.i("LEVEL1","a6");
		} catch(JSONException e){
			e.printStackTrace();
		}
		
		Log.i("LEVEL1","p1");
		mOptions = getResources().getStringArray(R.array.drawer_options);
		mDrawerList = (ListView) findViewById(R.id.drawer_list);
		Log.i("LEVEL1","p2");
		mDrawer = (LinearLayout) findViewById(R.id.drawer_linearlayout);
		mList = new ArrayList<HashMap<String,String>>();
		for(int i=0;i<TAM;i++){
			HashMap<String, String> hm = new HashMap<String,String>();
			hm.put(NAME, mOptions[i]);
			hm.put(ICON, Integer.toString(mFlags[i]) );
			mList.add(hm);
		}
		Log.i("LEVEL1","p3");
		/// Keys used in Hashmap
		String[] from = { ICON,NAME };
		//Ids of views in item_layout
		int[] to = { R.id.drawer_option_icon , R.id.drawer_option_name };
		// Instantiating an adapter to store each items
		mAdapter = new SimpleAdapter(this, mList, R.layout.draweritem, from, to);
		// Getting reference to DrawerLayout
		mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer);
		// Creating a ToggleButton for NavigationDrawer with drawer event listener
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer , R.string.drawer_open,R.string.drawer_close){
		 // Called when drawer is closed 
		 public void onDrawerClosed(View view) {
			 //highlightSelectedCountry();
			 invalidateOptionsMenu();
		 }
		// Called when a drawer is opened 
		 public void onDrawerOpened(View drawerView) {
			 //getSupportActionBar().setTitle("Select a Country");
			 invalidateOptionsMenu();
		 }
		 };
		 // Setting event listener for the drawer
		 mDrawerLayout.setDrawerListener(mDrawerToggle);
		 // ItemClick event handler for the drawer items
		 mDrawerList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			 public void onItemClick(AdapterView<?> arg0, View arg1, int drawerposition,
			 long arg3) {
			 mDrawerList.setItemChecked(drawerposition, false);
			 // Closing the drawer
			 mDrawerLayout.closeDrawer(mDrawer);
			// Intent - Calling the correspondent option Activity
             Log.i("LEVEL1","Vou configurar o intent de chamada da Activity da opcao");
             	 Intent i;
	             switch (drawerposition) {
	             case 0:  //perfil
	            	 	  i = new Intent(getApplicationContext(), ProfileActivity.class);
				  		  i.putExtra("position_drawer", drawerposition);
				  		  i.putExtra("option_name", mOptions[drawerposition]);
				  		  startActivity(i);
				  		  break;
	             case 1:  //PRINCIPAL, NADA
	                      break;
	             case 2:  //cenarios
	            	 	  i = new Intent(getApplicationContext(), ScenesActivity.class);
	            	 	  i.putExtra("position_drawer", drawerposition);
	            	 	  i.putExtra("option_name", mOptions[drawerposition]);
	            	 	  startActivity(i);
	                      break;
	             case 3:  //search
	            	 	  Toast.makeText(getBaseContext(),R.string.search_disable,Toast.LENGTH_LONG).show();
           	 	  		  break;
	             case 4:  //sobre
	            	 	  i = new Intent(getApplicationContext(), AboutActivity.class);
	            	 	  i.putExtra("position_drawer", drawerposition);
	            	 	  i.putExtra("option_name", mOptions[drawerposition]);
	            	 	  startActivity(i);
	            	 	  break;
	             case 5:  //configuracoes
		           	 	  i = new Intent(getApplicationContext(), QuickPrefsActivity.class);
		           	 	  i.putExtra("position_drawer", drawerposition);
		           	 	  i.putExtra("option_name", mOptions[drawerposition]);
		           	 	  startActivity(i);
		                  break;
	             case 6:  //desconectar
	            	 	  HTTPRequest.logoff(); //para desconectar com o servidor
	            	 	  i = new Intent(getApplicationContext(), LoginActivity.class);
				          i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				          startActivity(i);
	            	 	  break;
	             default: i = new Intent(getApplicationContext(), TutorialActivity.class);
	 					  i.putExtra("position_drawer", drawerposition);
	 					  i.putExtra("option_name", mOptions[drawerposition]);
	 					  startActivity(i);
	                      break;
	             } 
             
             	/*if(position==1){
	             //nada
	             } else {
	            	Intent i = new Intent(getApplicationContext(), TutorialActivity.class);
	 				i.putExtra("position_drawer", position);
	 				i.putExtra("option_name", mOptions[position]);
	 				startActivity(i);
	             }*/
				
			 }
		 });
		// Enabling Home button
	    actionBar.setHomeButtonEnabled(true);
	    // Enabling Up Navigation
	    actionBar.setDisplayHomeAsUpEnabled(true);
		// Setting the adapter to the listView
		mDrawerList.setAdapter(mAdapter);
		
		//Colocar um Listener na linha de identificacao do usuario para nao fazer nada
		LinearLayout userInfoDrawerRow = (LinearLayout) findViewById(R.id.linearlayout_profile);
		userInfoDrawerRow.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			}
		});
		//Navigation Drawer 2 End 
		
		Log.i("LEVEL1","vai colocar fragmento");

		//AQUI O CODIGO PARA O FRAGMENTO
		//Setando o Fragmento ABOUT
		Level1Fragment myFragment = new Level1Fragment();
		 
		//Enviando para o Fragmento
		Bundle data = new Bundle();
        data.putString(ARG_SECTION, "home");
        myFragment.setArguments(data);
        
        //Colocando Fragmento
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.mmmmll, myFragment);
        ft.commit();

        Log.i("LEVEL1","vai sair de onCreate()!!!");
       
	    //mHandler = new Handler();
	    //mHandler.postDelayed(mRunnable,5000);
	    
	    

	}


	//Attention: ON RESUME: After a pause OR at startup
	//Attention: ON RESTART: 
	
	/*@Override
	public void onPause() { 
		super.onPause();
		mHandler.removeCallbacks(mRunnable);
	}
	
	@Override
	public void onRestart() { 
		//Example: when pressed BACK button, the activity on the stack is restarted
	    super.onRestart();
	    Log.i("LEVEL1 - OnRestart()","ESTOU ENTRANDO NO ONRESTART()!!!");
	    //Refresh your stuff here
	    //Do the same thing this Activity does on OnCreate()
	    mHandler = new Handler();
	    mHandler.postDelayed(mRunnable,5000);
	    //End of refresh here
	}*/
	
	
	@Override
	// Add items to the ActionBar if it is presented
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_actions, menu);
		return super.onCreateOptionsMenu(menu);
	}

	
	//methods
	@Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }
	
	//More Methods related to Navigation Drawer
	// Handling the touch event of app icon
    @Override
    public boolean onOptionsItemSelected(MenuItem item) { 
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_favorite:
            	//Toast.makeText(getBaseContext(),"Cenas Favoritas!!! ",Toast.LENGTH_LONG).show();
            	Intent i = new Intent(getApplicationContext(), FavoriteScenesActivity.class);
 	 	  		startActivity(i);
            	return true;
            case R.id.action_refresh:
            	Toast.makeText(getBaseContext(),R.string.refresh_message,Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_voice:
            	Toast.makeText(getBaseContext(),R.string.voiceinteraction_disable,Toast.LENGTH_LONG).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    
    
    
    /* // Related to Navigation Drawer 1
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
    	// Called whenever we call invalidateOptionsMenu() 
        // If the drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    } */
    

    

    //SPINNER
    @Override
    public boolean onNavigationItemSelected(int itemPosition, long itemId) {
        // TODO Auto-generated method stub
        //Toast.makeText(getApplicationContext(), "text "+itemPosition+" "+itemId, Toast.LENGTH_LONG).show();
    	//AQUI O CODIGO PARA O FRAGMENTO
    	Level1Fragment myFragment = new Level1Fragment();
    	if(itemPosition==0){
    		//Enviando para o Fragmento
      		Bundle data = new Bundle();
            data.putString(ARG_SECTION, "home");
            myFragment.setArguments(data);
    	}
    	if(itemPosition==1){
    		//Enviando para o Fragmento
      		Bundle data = new Bundle();
            data.putString(ARG_SECTION, "appliances");
            myFragment.setArguments(data);
    	}
    	if(itemPosition==2){
    		//Enviando para o Fragmento
      		Bundle data = new Bundle();
            data.putString(ARG_SECTION, "others");
            myFragment.setArguments(data);
    	}
    	
          //Colocando Fragmento
          FragmentManager fragmentManager = getFragmentManager();
          FragmentTransaction ft = fragmentManager.beginTransaction();
          ft.replace(R.id.mmmmll, myFragment);
          ft.commit();

        return true;
    }
    
  
    
    
    
    
    /*private final Runnable mRunnable = new Runnable()
    {
        public void run()

        {
        	Toast.makeText(getApplicationContext(),"In Runnable!", Toast.LENGTH_SHORT).show();
        	mHandler.postDelayed(mRunnable, 5000);            
        }

    };//runnable*/
    
    //ERRO AO CHAMAR ESSA FUNCAO NESSA ACTIVITY!!!
	 // Related to Navigation Drawer 2
    private Bitmap createRoundImagex(Bitmap img){
		Bitmap circleBitmap = Bitmap.createBitmap(img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);
	    BitmapShader shader = new BitmapShader(img, TileMode.CLAMP, TileMode.CLAMP);
	    Paint paint = new Paint();
	    paint.setAntiAlias(true);
	    paint.setShader(shader);
	    Log.i("ABOUT","P14");
	    Canvas c = new Canvas(circleBitmap);
	    c.drawCircle(img.getWidth() / 2, img.getHeight() / 2, img.getWidth() / 2, paint);
	    return circleBitmap;
	}
    
    
    // Related to Navigation Drawer 2
    private Bitmap createRoundImageX(Bitmap img){
		Bitmap circleBitmap = Bitmap.createBitmap(img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);
	    BitmapShader shader = new BitmapShader(img, TileMode.CLAMP, TileMode.CLAMP);
	    Paint paint = new Paint();
	    paint.setAntiAlias(true);
	    paint.setShader(shader);
	    Log.i("ABOUT","P14");
	    Canvas c = new Canvas(circleBitmap);
	    c.drawCircle(img.getWidth() / 2, img.getHeight() / 2, img.getWidth() / 2, paint);
	    return circleBitmap;
	}

    
}
