package pairg.handyhome;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class Oraculo {

	// atributos
	private static int dual_ToggleButton = 1;
	private static int granular_SeekBar = 2;
	private static int gradual_TwoButtonsAndVisor = 3;
	private static int SimpleButton = 4;
	private static int options_Spinner = 5;
	private static int Button_withString = 6;
	private static int Button_withString_withStatus = 7;
	private static int Button_withdate_Picker = 8;
	private static int Button_withhour_Picker = 9;
	private static int Button_withResponseActivity = 10; //ESSE DAQUI PRECISA DE UMA FUNCAO PARA DIZER OS TIPOS DE INTERFACE, E OS VALORES PARA PREENCHER CADA TIPO 
	private static int Button_withFileInput = 11;
	private static int Button_withReturn = 12; //Precisa saber o tipo do retorno
	
	public static int getInterface(Appliance ap, int x) {
	//Metodo para retornar o tipo de interface do servi�o
	Log.i("ORACULO", "ORACULO PROCESSANDO...");
		
		//Descobrir o elemento de interface para o servi�o da posicao x
		//Pega o servico
		Service serv = ap.getService(x);
		//Pega o input_type
		ArrayList<String> input_type = serv.getInput_type();
		//Se TAMANHO de input_type for > 1, entao tem varios parametros. (� um bot�o que abre uma tela)
		if(input_type.size()>1){
			Log.i("ORACULO", "Button_withResponseActivity");
			return Button_withResponseActivity;
		}
		else{ //senao (tam <= 1) so tem 1 parametro
			
			//se input_type={null}, ou � um botao simples ou bot�o com retorno
			if(input_type.get(0).equals("null")){
				if(serv.hasReturn_type()){
					Log.i("ORACULO", "Resposta: Button_withReturn");
					return Button_withReturn;
				}
				else{
					Log.i("ORACULO", "Resposta: SimpleButton");
					return SimpleButton;
				}
			}
			else{
			//ent�o � algum elemento com interface de controle	
				
				if(input_type.get(0).equals("boolean")){
					Log.i("ORACULO", "Resposta: dual");
					return dual_ToggleButton;
				}
				if(input_type.get(0).equals("int_range_granular")){
					Log.i("ORACULO", "Resposta: granular");
					return granular_SeekBar;
				}
				if(input_type.get(0).equals("int_range_gradual")){
					Log.i("ORACULO", "Resposta: gradual");
					return gradual_TwoButtonsAndVisor;
				}
				if(input_type.get(0).equals("option")){
					Log.i("ORACULO", "Resposta: options");
					return options_Spinner;
				}
				if(input_type.get(0).equals("string")){
					if(serv.getStatus_id().get(0).equals("null")){
						Log.i("ORACULO", "Resposta: Button_withString");
						return Button_withString;
					}
					else{
						Log.i("ORACULO", "Resposta: Button_withString_withStatus");
						return Button_withString_withStatus;
					}
				}
				if(input_type.get(0).equals("date")){
					Log.i("ORACULO", "Resposta: date");
					return Button_withdate_Picker;
				}
				if(input_type.get(0).equals("hour")){
					Log.i("ORACULO", "Resposta: hour");
					return Button_withhour_Picker;
				}
				if(input_type.get(0).equals("file")){
					Log.i("ORACULO", "Resposta: file");
					return Button_withFileInput;
				}
				else{
					//Se n�o � nenhum desses ent�o, ERRO
					Log.i("ORACULO", "ERRO!!! N�o � nenhum desses tipos!");
					return 0;
				}
				
			}
			
		}
	}
	


	
	public static ArrayList<Integer> getElementsFromResponseActivity(Appliance ap, int x) {
		
		
		
		return new ArrayList();
	}
	
	
	
	
	public static ToggleButtonValues getToggleButtonValues(Appliance ap, int x) {
	//Metodo para retornar valor do ToggleButton (True or False)
	//Estou assumindo que esse metodo so ser� chamado caso getInterface() tenha retornado dual_ToggleButton (1)
	Log.i("ORACULO", "getToggleButtonValues");
		boolean value;
		String namefalse;
		String nametrue;
	
		//Pega o servico
		//Log.i("ORACULO - getToggleButtonValues", "Pega o servico");
		Service serv = ap.getService(x);
		//Pega o id do status
		//Log.i("ORACULO - getToggleButtonValues", "Pega o id do status");
		String sid = serv.getStatus_id().get(0);
		Log.i("ORACULO - getToggleButtonValues", "Pegou o status"+sid);
		
		//Pegar o Status que tenha esse statusid
		Log.i("ORACULO - getToggleButtonValues", "Pega o status");
		Status status = ap.getStatusById(sid);
			
		//Preencher valores de ToggleValues
		//Pegar o valor booleano atual (true or false)
		Log.i("ORACULO - getToggleButtonValues", "Pega o valor booleano");
		value = status.getValueBoolean();
		//Pegar o nome quando for false
		Log.i("ORACULO - getToggleButtonValues", "Pega o nome falso");
		namefalse = status.getName_false();
		//Pegar o nome quando for true
		Log.i("ORACULO - getToggleButtonValues", "Pega o nome verdadeiro");
		nametrue = status.getName_true();
		
		return new ToggleButtonValues(value, namefalse, nametrue);
	}

	public static ArrayList<Integer> getSeekBarOrTwoButtonsAndVisorValues(Appliance ap, int x) {
	Log.i("ORACULO", "getSeekBarOrTwoButtonsAndVisorValues()");
	ArrayList<Integer> retorno = new ArrayList();
	
		//Pega o servico
		//Log.i("ORACULO - getToggleButtonValues", "Pega o servico");
		Service serv = ap.getService(x);
		//Pega o id do status
		//Log.i("ORACULO - getToggleButtonValues", "Pega o id do status");
		String sid = serv.getStatus_id().get(0);
		Log.i("ORACULO - getToggleButtonValues", "Pegou o status"+sid);
			
		//Pegar o Status que tenha esse statusid
		Log.i("ORACULO - getToggleButtonValues", "Pega o status");
		Status status = ap.getStatusById(sid);
		
		//Preencher valores para retorno
		//Pegar o valor inteiro atual
		retorno.add(status.getValueInt());
		//Pegar o range 1
		retorno.add(status.getRange1());
		//Pegar o range 2
		retorno.add(status.getRange2());
		
		return retorno;
	}
	
	public static ArrayList<String> getSpinnerValues(Appliance ap, int x) {
	Log.i("ORACULO", "getSpinnerValues()");
	String value;
	ArrayList<String> alloptions;
	String selectedvalue;
	ArrayList<String> retorno = new ArrayList();
		
		//Pega o servico
		//Log.i("ORACULO - getToggleButtonValues", "Pega o servico");
		Service serv = ap.getService(x);
		//Pega o id do status
		//Log.i("ORACULO - getToggleButtonValues", "Pega o id do status");
		String sid = serv.getStatus_id().get(0);
		Log.i("ORACULO - getToggleButtonValues", "Pegou o status"+sid);
			
		//Pegar o Status que tenha esse statusid
		Log.i("ORACULO - getToggleButtonValues", "Pega o status");
		Status status = ap.getStatusById(sid);
			
		//Preencher valores de OptionsValues
		//Pegar o valor da string atual
		selectedvalue = status.getValueString();
		//Pegar opcoes
		alloptions = status.getOptions();
			
		retorno.add(selectedvalue);
		for(int i=0; i < alloptions.size(); i++){
			if(alloptions.get(i).equals(selectedvalue)){ //mudar depois	
			}
			else{
				retorno.add(alloptions.get(i));
			}
		}
				
		return retorno;
	}
	
	public static ArrayList<Integer> getDateValues(Appliance ap, int x) {
	Log.i("ORACULO", "getDateValues()");
		
		//Pega o servico
		//Log.i("ORACULO - getToggleButtonValues", "Pega o servico");
		Service serv = ap.getService(x);
		//Pega o id do status
		//Log.i("ORACULO - getToggleButtonValues", "Pega o id do status");
		String sid = serv.getStatus_id().get(0);
		Log.i("ORACULO - getToggleButtonValues", "Pegou o status"+sid);
			
		//Pegar o Status que tenha esse statusid
		Log.i("ORACULO - getToggleButtonValues", "Pega o status");
		Status status = ap.getStatusById(sid);
			
		//Pegar o valor de data atual que est� no ArrayList<Integer>
		return status.getValueArrayInt();
	}
	
	public static ArrayList<Integer> getHourValues(Appliance ap, int x) {
	Log.i("ORACULO", "getHourValues()");
		
		//Pega o servico
		//Log.i("ORACULO - getToggleButtonValues", "Pega o servico");
		Service serv = ap.getService(x);
		//Pega o id do status
		//Log.i("ORACULO - getToggleButtonValues", "Pega o id do status");
		String sid = serv.getStatus_id().get(0);
		Log.i("ORACULO - getToggleButtonValues", "Pegou o status"+sid);
			
		//Pegar o Status que tenha esse statusid
		Log.i("ORACULO - getToggleButtonValues", "Pega o status");
		Status status = ap.getStatusById(sid);
			
		//Pegar o valor da hora atual que est� no ArrayList<Integer>
		return status.getValueArrayInt();
	}
	
	public static String getStringStatusValue(Appliance ap, int x) {
		Log.i("ORACULO", "getStringStatusValue()");
		
		//Pega o servico
		//Log.i("ORACULO - getToggleButtonValues", "Pega o servico");
		Service serv = ap.getService(x);
		//Pega o id do status
		//Log.i("ORACULO - getToggleButtonValues", "Pega o id do status");
		String sid = serv.getStatus_id().get(0);
		Log.i("ORACULO - getToggleButtonValues", "Pegou o status"+sid);
				
		//Pegar o Status que tenha esse statusid
		Log.i("ORACULO - getToggleButtonValues", "Pega o status");
		Status status = ap.getStatusById(sid);
		
		return status.getValueString();
	}
	
	/*public static ArrayList<String> getReturnValues(Appliance ap, int x) {
		
		
		return x;
	}*/
	
}
















class ToggleButtonValues{
	// atributos
	private boolean value;
	private String name_true;
	private String name_false;
	
	// construtor
	public ToggleButtonValues(boolean b, String namefalse, String nametrue){
		this.value = b;
		this.name_false = namefalse;
		this.name_true = nametrue;
	}
	
	public boolean getValue(){
		return this.value;
	}
	
	public String getNameFalse(){
		return this.name_false;
	}
	
	public String getNameTrue(){
		return this.name_true;
	}
}








