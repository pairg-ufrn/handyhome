package pairg.handyhome;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
 
public class AboutFragment extends Fragment{
 
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
 
        /*// Se quiser pegar os itens enviados no campo Extra
        int extraposition = getArguments().getInt("position_drawer");
        String extraname = getArguments().getString("option_name");
        // List of options
        String[] my_options = getResources().getStringArray(R.array.drawer_options);*/
 
        
        // Creating view correspoding to the fragment
        View v = inflater.inflate(R.layout.fragment_about, container, false);
 
        //Se quiser alterar a vers�o
        TextView tv = (TextView) v.findViewById(R.id.aboutappversion);
        tv.setText("Vers�o 1.0.1");
 
        // Updating the action bar title
        //getActivity().getActionBar().setTitle(rivers[position]);
        return v;
    }
}

