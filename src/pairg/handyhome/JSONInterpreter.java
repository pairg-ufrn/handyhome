package pairg.handyhome;

import pairg.handyhome.Group;
import pairg.handyhome.Appliance;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader.TileMode;
import android.util.Log;

public class JSONInterpreter {
	private static String TAG = JSONInterpreter.class.getName();
	private static Resources res;

	public static void setResources(Resources x){
		res = x;
	}

	
	
	
	//MAP JSON TO GROUPS
	public static ArrayList<Group> mapJSONtoGroups(JSONObject jObj) throws JSONException{
		ArrayList<Group> groups = new ArrayList<Group>();
		
		Log.i("JSONINTERPRETER - mapJSONtoGroups","Entrei!!!");
		Log.i("JSONINTERPRETER - mapJSONtoGroups","groups.size() = "+groups.size());
		
		// Extrai content do JSON
		JSONArray jGroups = jObj.getJSONArray("content");
		int n = jGroups.length();
		Log.i("JSONINTERPRETER - mapJSONtoGroups", "n = " + Integer.toString(n));
		
		// Percorre o JSONArray instanciando objetos Group para cada index
		Log.i("JSONINTERPRETER - mapJSONtoGroups","Vou entrar no for");
		for(int i = 0; i < n; i++){
			// Cria um JSONOBject temporário para instanciar o Group
			JSONObject jTemp = jGroups.getJSONObject(i);
			
			// Mapeia o JSONObject de index 'i' em um objeto Group
			String id = jTemp.getString("id");
			int objects = jTemp.getInt("objects");
			
			String name = jTemp.getJSONObject("visual").getString("name");
			String image_url = jTemp.getJSONObject("visual").getString("image");
			String image_name = image_url.substring( image_url.lastIndexOf('/')+1, image_url.length() );
			
			Log.i("JSONINTERPRETER","Objeto["+i+"]"+" = name:"+name+", image:"+image_name);
			
			//Enquanto nao tiver BD, chamar fun��o da imagem encapsulada simulaImagem
			Bitmap image = simulaImagem(image_name);
			
			Log.i("JSONINTERPRETER","Saiu do SimulaImagem!!! Vou instanciar gtemp. Vou adicionar no array groups");
			Group gtemp = new Group(id, image, name, objects);
			groups.add(gtemp);
			Log.i("JSONINTERPRETER","groups.size() = "+groups.size());
		}		
		return groups;
		
	}
	
	//MAP JSON TO APPLIANCES
	public static ArrayList<Appliance> mapJSONtoAppliances(JSONObject jObj) throws JSONException{
		ArrayList<Appliance> appliances = new ArrayList<Appliance>();
		
		Log.i("JSONINTERPRETER - mapJSONtoApliances","Entrei!!!");
		Log.i("JSONINTERPRETER - mapJSONtoApliances","appliances.size() = "+appliances.size());
		
		// Extrai content do JSON
		JSONArray jAppliances = jObj.getJSONArray("content");
		int n = jAppliances.length();
		Log.i("JSONINTERPRETER - mapJSONtoApliances", "n = " + Integer.toString(n));
		
		// Percorre o JSONArray instanciando objetos Appliance para cada index
		Log.i("JSONINTERPRETER - mapJSONtoApliances","Vou entrar no for");
		for(int i = 0; i < n; i++){
			// Cria um JSONOBject temporário para instanciar a Apliance
			JSONObject jTemp = jAppliances.getJSONObject(i);
			
			// Mapeia o JSONObject de index 'i' em um objeto Appliance
			String id = jTemp.getString("id");
			String kind = jTemp.getString("kind");
			String type = jTemp.getString("type");
			
			JSONObject jObjVisual  = jTemp.getJSONObject("visual");
			String visual_id = jObjVisual.getString("id");
			String visual_name = jObjVisual.getString("name");
			String image_url = jObjVisual.getString("image");
			String image_name = image_url.substring( image_url.lastIndexOf('/')+1, image_url.length() );
			
			//Enquanto nao tiver BD, chamar fun��o da imagem encapsulada simulaImagem
			Bitmap visual_image = simulaImagem(image_name);
			//gambiarra
			Bitmap image = simulaImagem("qualquercoisa");
			
			Log.i("JSONINTERPRETER - mapJSONtoApliances","Appliance["+i+"]"+" = id:"+id);
			
			// Instancia um appliance com os dados do JSONArray
			Log.i("JSONINTERPRETER - mapJSONtoApliances","Vou instanciar atemp. Vou adicionar no array appliances");
			Appliance atemp = new Appliance(id, kind, type, visual_id, visual_name, visual_image, jTemp.getJSONArray("services"), jTemp.getJSONArray("status"));
			appliances.add(atemp);
			Log.i("JSONINTERPRETER - mapJSONtoApliances","appliances.size() = "+appliances.size());
		}
		return appliances;
	}
	
	
	//MAP JSON TO SCENES (Incomplete)
	public static ArrayList<Scene> mapJSONtoScenes(JSONObject jObj) throws JSONException{
		ArrayList<Scene> scenes = new ArrayList<Scene>();
		
		Log.i("JSONINTERPRETER - mapJSONtoScenes","Entrei!!!");
		Log.i("JSONINTERPRETER - mapJSONtoScenes","scenes.size() = "+scenes.size());
		
		// Extrai content do JSON
		JSONArray jAppliances = jObj.getJSONArray("content");
		int n = jAppliances.length();
		Log.i("JSONINTERPRETER - mapJSONtoScenes", "n = " + Integer.toString(n));
		
		// Percorre o JSONArray instanciando objetos Appliance para cada index
		Log.i("JSONINTERPRETER - mapJSONtoScenes","Vou entrar no for");
		for(int i = 0; i < n; i++){
			// Cria um JSONOBject temporario para instanciar a Scene
			JSONObject jTemp = jAppliances.getJSONObject(i);
			
			// Mapeia o JSONObject de index 'i' em um objeto Scene
			String id = jTemp.getString("id");
			Log.i("JSONINTERPRETER - mapJSONtoScenes","Scene["+i+"]"+" = id:"+id);
			JSONObject jObjVisual  = jTemp.getJSONObject("visual");
			String visual_id = jObjVisual.getString("id");
			String visual_name = jObjVisual.getString("name");
			String image_url = jObjVisual.getString("image");
			String image_name = image_url.substring( image_url.lastIndexOf('/')+1, image_url.length() );
			
			//Enquanto nao tiver BD, chamar fun��o da imagem encapsulada simulaImagem
			Bitmap visual_image = simulaImagem(image_name);

			// Instancia um appliance com os dados do JSONArray
			Log.i("JSONINTERPRETER - mapJSONtoScenes","Vou instanciar stemp. Vou adicionar no array scenes");
			Scene stemp = new Scene(image_name, id, visual_id, visual_name, visual_image, jTemp.getJSONArray("actions"));
			scenes.add(stemp);
			Log.i("JSONINTERPRETER - mapJSONtoScenes","appliances.size() = "+scenes.size());
		}
		return scenes;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	//CLASSE ATUALMENTE ESTATICA, nao pega informacoes do JSON
	public static ArrayList<String> mapJSONtoUserInfo(JSONObject jObj) {
		ArrayList<String> retorno = new ArrayList();
		retorno.add("Sarah Sakamoto");
		retorno.add("fotousuario.png");
		retorno.add("sarahsakamoto");
		retorno.add("13/01/2014");
		retorno.add("Nenhuma");
		return retorno;
	}
		
	
	//CLASSE ATUALMENTE ESTATICA, nao pega informacoes do JSON
	public static ArrayList<String> mapJSONtoUserBasicInfo(JSONObject jObj) {
		ArrayList<String> retorno = new ArrayList();
		Log.i("JSONInterpreter","mapJSONtoUserBasicInfo");
		retorno.add("Sarah Sakamoto");
		retorno.add("fotousuario.png");
		return retorno;
	}
	
	
	
	
	
	
	
	
	
	
	//Funcao que simula pegar um Bitmap do BD
	//OBS: Ao deletar esta fun��o como est� atualmente ou se refatora-la para IFs est�ticos, deletar tamb�m a vari�vel da classe temp e o m�todo setBitmap(Bitmap t)
	public static Bitmap simulaImagem(String image_name){
		Log.i("SIMULAIMAGEM","Entrou no SimulaImagem!!!");
		//Nao consegui fazer os IFs pois n�o reconhecia "this.getResources()", pois JSONInterpreter n�o pega o contexto da Activity.
		//Bitmap salaIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.ic_menu_lampadas);
		//return temp;
		Bitmap t = BitmapFactory.decodeResource(res, R.drawable.ic_menu_default);
		
		//groups
		if (image_name.equals("sala1.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_menu_sala1);
		}
		if (image_name.equals("quarto1.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_menu_quarto1);
		}
		if (image_name.equals("banheiro1.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_menu_banheiro1);
		}
		if (image_name.equals("cozinha1.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_menu_cozinha1);
		}
		if (image_name.equals("varanda1.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_menu_varanda1);
		}
		if (image_name.equals("lavanderia1.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_menu_lavanderia1);
		}
		if (image_name.equals("biblioteca1.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_menu_biblioteca1);
		}
		if (image_name.equals("escritorio1.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_menu_escritorio1);
		}
		if (image_name.equals("garagem1.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_menu_garagem1);
		}
		if (image_name.equals("quartobeliche1.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_menu_quartobeliche1);
		}
		if (image_name.equals("quartocasal1.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_menu_quartocasal1);
		}
		if (image_name.equals("quartocasal2.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_menu_quartocasal2);
		}
		if (image_name.equals("biblioteca2.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_menu_biblioteca2);
		}
		if (image_name.equals("sala2.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_menu_sala2);
		}
		if (image_name.equals("comoda1.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_menu_comoda1);
		}
		if (image_name.equals("comoda2.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_menu_comoda2);
		}
		if (image_name.equals("garagem2.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_menu_garagem2);
		}
		if (image_name.equals("quarto2.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_menu_quarto2);
		}
		if (image_name.equals("lampadas1.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_menu_lampadas1);
		}
		if (image_name.equals("portas1.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_menu_portas1);
		}
		if (image_name.equals("janelas1.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_menu_janelas1);
		}
		if (image_name.equals("chuveiros1.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_menu_chuveiros1);
		}
		if (image_name.equals("arcondicionado1.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_menu_arcondicionado1);
		}
		if (image_name.equals("luminaria1.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_menu_luminaria1);
		}
		
		//objects
		if (image_name.equals("lampada_off.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_menu_lampada_off);
		}
		if (image_name.equals("lampada_on.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_menu_lampada_on);
		}
		if (image_name.equals("janela_open.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_menu_janela_open);
		}
		if (image_name.equals("porta_open.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_menu_porta_open);
		}
		
		//cenas
		if (image_name.equals("ic_cena_acordar.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_cena_acordar);
		}
		if (image_name.equals("ic_cena_sair.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_cena_sair);
		}
		if (image_name.equals("ic_cena_chovendo.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_cena_chovendo);
		}
		if (image_name.equals("ic_cena_lavarroupa.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_cena_lavarroupa);
		}
		if (image_name.equals("ic_cena_secarroupa.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_cena_secarroupa);
		}
		if (image_name.equals("ic_cena_trabalho.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_cena_trabalho);
		}
		if (image_name.equals("ic_cena_banho.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_cena_banho);
		}
		if (image_name.equals("ic_cena_viagem.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_cena_viagem);
		}
		if (image_name.equals("ic_cena_festa.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_cena_festa);
		}
		if (image_name.equals("ic_cena_cinema.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_cena_cinema);
		}
		if (image_name.equals("ic_cena_musica.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_cena_musica);
		}
		if (image_name.equals("ic_cena_dormir.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_cena_dormir);
		}
		
		//others
		if (image_name.equals("imagemx.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_menu_service_default);
		}
		
		//drawer
		if (image_name.equals("fotousuario.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.fotousuario);
		}
		if (image_name.equals("fotousuario2.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.fotousuario2);
		}
		
		if (image_name.equals("ic_cena_favorita0.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_cena_favorita0);
		}
		if (image_name.equals("ic_cena_favorita1.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_cena_favorita1);
		}
		if (image_name.equals("ic_cena_favorita2.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_cena_favorita2);
		}
		if (image_name.equals("ic_cena_favorita3.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_cena_favorita3);
		}
		if (image_name.equals("ic_cena_favorita4.png")){
			t = BitmapFactory.decodeResource(res, R.drawable.ic_cena_favorita4);
		}
		
		
		Log.i("SIMULAIMAGEM","Saindo do SimulaImagem!!!");
		return t;
	}
	
	//para acesso de classes externas
	public static Bitmap getImageFromName(String image_name){
		Bitmap x = simulaImagem(image_name);
		return x;
	}
	
	//para acesso da classe Main
	public static int getImageResIdFromNameAndContext(String imgname, Context context){
		int id = 0;
		if(imgname.equals("fotousuario.png")){
			id = context.getResources().getIdentifier("fotousuario.png", "drawable", context.getPackageName());
		}
		if(imgname.equals("fotousuario2.png")){
			id = context.getResources().getIdentifier("fotousuario2.png", "drawable", context.getPackageName());
		}
		return id;
	}

}
