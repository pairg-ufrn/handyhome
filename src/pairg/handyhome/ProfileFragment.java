package pairg.handyhome;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader.TileMode;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
 
public class ProfileFragment extends Fragment{
 
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
 
        /*// Se quiser pegar os itens enviados no campo Extra
        int extraposition = getArguments().getInt("position_drawer");
        String extraname = getArguments().getString("option_name");
        // List of options
        String[] my_options = getResources().getStringArray(R.array.drawer_options);*/
 
        
        // Creating view correspoding to the fragment
        View v = inflater.inflate(R.layout.fragment_profile, container, false);
 
        
        ArrayList<String> userinfo = new ArrayList();
        try{
			// Pede o JSONObject 
			JSONObject myUserJSON = HTTPRequest.getUserInfo();
			userinfo = JSONInterpreter.mapJSONtoUserInfo(myUserJSON);
			
		} catch(JSONException e){
			Log.i("Profile Fragment","Exception!!!");
			e.printStackTrace();
		}
        
        //Para alterar os dados
        TextView username = (TextView) v.findViewById(R.id.profileusername);
		username.setText(userinfo.get(0));
        
        ImageView userimage = (ImageView) v.findViewById(R.id.profileuserimage);
        userimage.setImageBitmap(createRoundImage(JSONInterpreter.getImageFromName(userinfo.get(1))));
        
        TextView nickname = (TextView) v.findViewById(R.id.profilenickname);
		nickname.setText(userinfo.get(2));
        
		TextView usuariodesde = (TextView) v.findViewById(R.id.profilesincedate);
		usuariodesde.setText(userinfo.get(3));
		
		TextView notificacoes = (TextView) v.findViewById(R.id.profilenotificacoes);
		notificacoes.setText(userinfo.get(4));

        // Updating the action bar title
        //getActivity().getActionBar().setTitle(rivers[position]);
        return v;
    }
    
    
    private Bitmap createRoundImage(Bitmap img){
		Bitmap circleBitmap = Bitmap.createBitmap(img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);
	    BitmapShader shader = new BitmapShader(img, TileMode.CLAMP, TileMode.CLAMP);
	    Paint paint = new Paint();
	    paint.setAntiAlias(true);
	    paint.setShader(shader);
	    Log.i("ABOUT","P14");
	    Canvas c = new Canvas(circleBitmap);
	    c.drawCircle(img.getWidth() / 2, img.getHeight() / 2, img.getWidth() / 2, paint);
	    return circleBitmap;
	}

    
}

