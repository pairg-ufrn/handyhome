package pairg.handyhome;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import pairg.handyhome.adapter.ListViewScenesAdapter;
import android.R.color;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Shader.TileMode;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class ScenesActivity extends Activity  {
    
	private ActionBar actionBar;
	ListView listView;
	ArrayList<Scene> myScenesArray =  new ArrayList<Scene>();
	ListViewScenesAdapter customListAdapter;
	
	//VARIAVEIS NavDrawer 2
			String[] mOptions ;
			private DrawerLayout mDrawerLayout;
			private ListView mDrawerList;
			private ActionBarDrawerToggle mDrawerToggle;
			private LinearLayout mDrawer ;
			private List<HashMap<String,String>> mList ;
			private SimpleAdapter mAdapter;
			final private String NAME = "option_name";
			final private String ICON = "option_icon";
			int[] mFlags = new int[]{
					 R.drawable.ic_drawer_profile,
					 R.drawable.ic_drawer_home,
					 R.drawable.ic_drawer_scenes,
					 R.drawable.ic_drawer_search,
					 R.drawable.ic_drawer_about,
					 R.drawable.ic_drawer_settings,
					 R.drawable.ic_drawer_exit
			};
			final private int TAM = mFlags.length;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        /*ListView list = new ListView(this);
        list.setBackgroundColor(Color.WHITE);
        setContentView(list);*/
        setContentView(R.layout.activity_scenes);
        
        // Initialization
     	actionBar = getActionBar();
     	actionBar.setDisplayShowTitleEnabled(false);
     	
     	/*// Enabling Home button
	    actionBar.setHomeButtonEnabled(true);
	    // Enabling Up Navigation
	    actionBar.setDisplayHomeAsUpEnabled(true);*/
        
	    ArrayList<String> items = new ArrayList();
	    try{
			// Pede o JSONObject
			Log.i("SCENES","Vou pegar o JSON das cenas");
			JSONObject myJSON = HTTPRequest.getScenes();
			
			Log.i("SCENES","Peguei o JSON de HTTPRequest");
			Log.i("SCENES","myScenesArray tam = "+myScenesArray.size());
			Log.i("SCENES","Agora vou para JSONInterpreter");
			
			myScenesArray = JSONInterpreter.mapJSONtoScenes(myJSON);
			
			Log.i("SCENES","Voltei do JSONInterpreter");
			Log.i("LEVEL2","myScenesArray tam: "+myScenesArray.size());
		}catch(JSONException e){
			e.printStackTrace();
		}
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	   
	  //Navigation Drawer 2 Init
		Log.i("SCENES","Iniciou Navigation Drawer");
		try{
			// Pede o JSONObject 
			Log.i("SCENES","a1");
			JSONObject myUserJSON = HTTPRequest.getUserBasicInfo();
			Log.i("SCENES","a2");
			ArrayList<String> userinfo = JSONInterpreter.mapJSONtoUserBasicInfo(myUserJSON);
			Log.i("SCENES","a3");
			TextView username = (TextView) findViewById(R.id.usernamedrawer5);
			Log.i("SCENES","a4");
			username.setText(userinfo.get(0));
			Log.i("SCENES","a5");
			ImageView userimage = (ImageView) findViewById(R.id.userimagedrawer5);
			Log.i("SCENES","a6");
			//userimage.setImageBitmap(createRoundImage(JSONInterpreter.getImageFromName(userinfo.get(1))));
			userimage.setImageBitmap(JSONInterpreter.getImageFromName(userinfo.get(1)));
			Log.i("SCENES","a7");
		} catch(JSONException e){
			Log.i("SCENES","Exception!!!");
			e.printStackTrace();
		}
		
		Log.i("SCENES","P1");
		mOptions = getResources().getStringArray(R.array.drawer_options);
		mDrawerList = (ListView) findViewById(R.id.drawer_list5);
		mDrawer = (LinearLayout) findViewById(R.id.drawer_linearlayout5);
		Log.i("SCENES","P2");
		mList = new ArrayList<HashMap<String,String>>();
		for(int i=0;i<TAM;i++){
			HashMap<String, String> hm = new HashMap<String,String>();
			hm.put(NAME, mOptions[i]);
			hm.put(ICON, Integer.toString(mFlags[i]) );
			mList.add(hm);
		}
		Log.i("SCENES","P3");
		/// Keys used in Hashmap
		String[] from = { ICON,NAME };
		//Ids of views in item_layout
		int[] to = { R.id.drawer_option_icon , R.id.drawer_option_name };
		Log.i("SCENES","P4");
		// Instantiating an adapter to store each items
		mAdapter = new SimpleAdapter(this, mList, R.layout.draweritem, from, to);
		// Getting reference to DrawerLayout
		mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer5);
		// Creating a ToggleButton for NavigationDrawer with drawer event listener
		Log.i("SCENES","P5");
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer , R.string.drawer_open,R.string.drawer_close){
		 // Called when drawer is closed 
		 public void onDrawerClosed(View view) {
			 //highlightSelectedCountry();
			 invalidateOptionsMenu();
		 }
		// Called when a drawer is opened 
		 public void onDrawerOpened(View drawerView) {
			 //getSupportActionBar().setTitle("Select a Country");
			 invalidateOptionsMenu();
		 }
		 };
		 Log.i("SCENES","P6");
		 // Setting event listener for the drawer
		 mDrawerLayout.setDrawerListener(mDrawerToggle);
		 // ItemClick event handler for the drawer items
		 Log.i("SCENES","P7");
		 mDrawerList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			 public void onItemClick(AdapterView<?> arg0, View arg1, int drawerposition,
			 long arg3) {
			 mDrawerList.setItemChecked(drawerposition, false);
			 Log.i("SCENES","P8");
			 // Closing the drawer
			 mDrawerLayout.closeDrawer(mDrawer);
			// Intent - Calling the correspondent option Activity
             Log.i("SCENES","Vou configurar o intent de chamada da Activity da opcao");
             	 Intent i;
	             switch (drawerposition) {
	             case 0:  //perfil
	            	 	  i = new Intent(getApplicationContext(), ProfileActivity.class);
				  		  i.putExtra("position_drawer", drawerposition);
				  		  i.putExtra("option_name", mOptions[drawerposition]);
				  		  startActivity(i);
				  		  break;
	             case 1:  //principal
	            	 	  i = new Intent(getApplicationContext(), MainActivity.class);
	            	 	  i.putExtra("position_drawer", drawerposition);
	            	 	  i.putExtra("option_name", mOptions[drawerposition]);
	            	 	  startActivity(i);
	            	 	  break;
	             case 2:  //CENARIOS, NADA (atualmente chamando tutorial)
	                      break;
	             case 3:  //search
	            	 	  Toast.makeText(getBaseContext(),R.string.search_disable,Toast.LENGTH_LONG).show();
           	 	  		  break;
	             case 4:  //sobre
	            	 	  i = new Intent(getApplicationContext(), AboutActivity.class);
	            	 	  i.putExtra("position_drawer", drawerposition);
	            	 	  i.putExtra("option_name", mOptions[drawerposition]);
	            	 	  startActivity(i);
	            	 	  break;
	             case 5:  //configuracoes
		           	 	  i = new Intent(getApplicationContext(), QuickPrefsActivity.class);
		           	 	  i.putExtra("position_drawer", drawerposition);
		           	 	  i.putExtra("option_name", mOptions[drawerposition]);
		           	 	  startActivity(i);
		                   break;
	             case 6:  //desconectar
	            	 	  HTTPRequest.logoff(); //para desconectar com o servidor
	            	 	  i = new Intent(getApplicationContext(), LoginActivity.class);
	            	 	  i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	            	 	  startActivity(i);
	            	 	  break;
	             default: i = new Intent(getApplicationContext(), TutorialActivity.class);
	 					  i.putExtra("position_drawer", drawerposition);
	 					  i.putExtra("option_name", mOptions[drawerposition]);
	 					  startActivity(i);
	                      break;
	             } 
             
	             //fim do switch
				
			 }
		 });
		 Log.i("SCENES","P9");
		// Enabling Home button
	    actionBar.setHomeButtonEnabled(true);
	    // Enabling Up Navigation
	    actionBar.setDisplayHomeAsUpEnabled(true);
		// Setting the adapter to the listView
		mDrawerList.setAdapter(mAdapter);
		
		//Colocar um Listener na linha de identificacao do usuario para nao fazer nada
		LinearLayout userInfoDrawerRow = (LinearLayout) findViewById(R.id.linearlayout_profile5);
		userInfoDrawerRow.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.i("SCENES","P10");
				// TODO Auto-generated method stub
			}
		});
		//Navigation Drawer 2 End
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    //preenchendo os valores do array de string (nomes das cenas)
        for(int i=0; i<myScenesArray.size(); i++){
        	items.add( (myScenesArray.get(i)).getVisualName() );
        }

        
        //Supply this adapter with R.layout.scenesrow
        /*ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.scenesrow, R.id.text, items) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View row =  super.getView(position, convertView, parent);
                ImageView left = (ImageView)row.findViewById(R.id.left);
                left.setImageBitmap( (myScenesArray.get(position)).getVisualImage() );
                //left.setTag(position);
                //left.setOnClickListener(ScenesActivity.this);
                View right = row.findViewById(R.id.right);
                right.setTag(position);
                right.setOnClickListener(ScenesActivity.this);
                
                return row;
            }
        };
        
        list.setAdapter(adapter);
        list.setOnItemClickListener(this);
        */
        
        
        Log.i("SCENES","P20");
        listView = (ListView) findViewById(R.id.sceneslistview);
        customListAdapter = new ListViewScenesAdapter(this.getBaseContext(), R.layout.scenesrow, myScenesArray);
        listView.setAdapter(customListAdapter);
        
        
        
        /*for(int pos=0; pos<myScenesArray.size(); pos++){
        	View myrow = listView.getView(pos);
        }*/
        
        
        listView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v, final int position, long id){
				Toast.makeText(getApplicationContext(),"As a��es dessa cena n�o podem ser detalhadas", Toast.LENGTH_SHORT).show();
				// Vibrate for 500 milliseconds
					//Vibrator vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
					//vib.vibrate(500);
				// Ring android notification sound
				//try {
			    //    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			    //    Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
			    //    r.play();
			    //} catch (Exception e) {}
				/*
				AlertDialog.Builder mensagem = new AlertDialog.Builder(ScenesActivity.this);
				mensagem.setTitle("Cena "+myScenesArray.get(position).getVisualName());
				mensagem.setMessage("Cena possui as seguintes a��es: X, Y, Z");
				//mensagem.setNeutralButton("OK",null);
				mensagem.setPositiveButton("Executar", new DialogInterface.OnClickListener() {
			           public void onClick(DialogInterface dialog, int id) {
			               // User clicked OK button
			        	   Toast.makeText(getBaseContext(),"Cena  \"" + myScenesArray.get(position).getVisualName() + "\"  executada!", Toast.LENGTH_SHORT).show();
			                if(MyHandyHome.getFVibrate()){
								Vibrator vib = (Vibrator) getSystemService(VIBRATOR_SERVICE);
								vib.vibrate(500);
							}
			           }
			       });
				mensagem.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
			           public void onClick(DialogInterface dialog, int id) {
			               // User cancelled the dialog
			           }
			       });
				AlertDialog dialog = mensagem.create();
				mensagem.show();*/
				
				// Chamar o OutraActivity
				// debug
				/*Log.i("LEVEL2FRAGMENT","Vou configurar o intent de chamada da Level3Activity");
				Intent i = new Intent(getApplicationContext(), Level3Activity.class);
				Log.i("LEVEL2FRAGMENT","item_text = " + myScenesArray.get(position).getId());
				//Aqui era para usar Serializable ou Parcelable no putExtra, mas como n�o funcionou, vai vari�vel static mesmo (na classe Transfer)!
				//i.putExtra("my_appliance", gridArray.get(position));
				i.putExtra("position_to_color", position);
				startActivity(i);*/
			}
		});
 
        
        
        
    }
    
    /*@Override
    public void onClick(View v) {
        switch(v.getId()) {
        case R.id.left:
            Toast.makeText(this, "Left Accessory "+v.getTag(), Toast.LENGTH_SHORT).show();
            break;
        case R.id.right:
            Toast.makeText(this, "Right Accessory "+v.getTag(), Toast.LENGTH_SHORT).show();
            break;
        default:
            break;
        }
    }
    
    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        Toast.makeText(this, "Item Click "+position, Toast.LENGTH_SHORT).show();
    }*/

    //MYMYMY
    
    
    
	@Override
	// Add items to the ActionBar if it is presented
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_actions, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	//methods
	@Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        Log.i("SCENES","P12");
        mDrawerToggle.syncState();
    }
	
	//More Methods related to Navigation Drawer
	// Handling the touch event of app icon
    @Override
    public boolean onOptionsItemSelected(MenuItem item) { 
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_favorite:
            	//Toast.makeText(getBaseContext(),"Cenas Favoritas!!! ",Toast.LENGTH_LONG).show();
            	Intent i = new Intent(getApplicationContext(), FavoriteScenesActivity.class);
 	 	  		startActivity(i);
            	return true;
            case R.id.action_refresh:
            	Toast.makeText(getBaseContext(),R.string.refresh_message,Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_voice:
            	Toast.makeText(getBaseContext(),R.string.voiceinteraction_disable,Toast.LENGTH_LONG).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    
 // Related to Navigation Drawer 2
    private Bitmap createRoundImage(Bitmap img){
		Bitmap circleBitmap = Bitmap.createBitmap(img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);
	    BitmapShader shader = new BitmapShader(img, TileMode.CLAMP, TileMode.CLAMP);
	    Paint paint = new Paint();
	    paint.setAntiAlias(true);
	    paint.setShader(shader);
	    Log.i("SCENES","P14");
	    Canvas c = new Canvas(circleBitmap);
	    c.drawCircle(img.getWidth() / 2, img.getHeight() / 2, img.getWidth() / 2, paint);
	    return circleBitmap;
	}

    




}
