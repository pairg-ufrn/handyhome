package pairg.handyhome;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import pairg.handyhome.adapter.GridViewLevel1Adapter;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
 
public class Level1Fragment extends Fragment {
 
	GridView gridView;
	ArrayList<Group> gridArray = new ArrayList<Group>();
	GridViewLevel1Adapter customGridAdapter;
	String ARG_SECTION = "section";
	
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
        Bundle savedInstanceState) {
 
        // Se quiser pegar os itens enviados no campo Extra
        String section = getArguments().getString(ARG_SECTION);
        
        try{
			Log.i("LEVEL1","Vou pegar o JSON - section: "+section);
			JSONObject myJSON = HTTPRequest.getGroups(section);
			Log.i("LEVEL1","Peguei o JSON de HTTPRequest");
			JSONInterpreter.setResources(this.getResources());
			// Interpreta JSONObject e gera os grupos
			Log.i("LEVEL1","Antes de ir para JSONInterpreter");
			Log.i("LEVEL1","gridArray tamamho = "+gridArray.size());
			Log.i("LEVEL1","Vou para JSONInterpreter");
			gridArray = JSONInterpreter.mapJSONtoGroups(myJSON);
			Log.i("LEVEL1","Adicionados no gridArray");
			Log.i("LEVEL1","gridArray tamanho: "+gridArray.size());
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        View fragmento = inflater.inflate(R.layout.fragment_level1, container, false);

        gridView = (GridView) fragmento.findViewById(R.id.gridViewTestL1);
		customGridAdapter = new GridViewLevel1Adapter(this.getActivity(), R.layout.group, gridArray);
		gridView.setAdapter(customGridAdapter);
		
		Log.i("LEVEL1","GridView, vai setar o listener");
		gridView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id){
				Toast.makeText(
						getActivity().getApplicationContext(),
						gridArray.get(position).getName(), 
						Toast.LENGTH_SHORT).show();

				// Chamar o Level2Activity
				// debug
				Log.i("LEVEL1","Vou configurar o intent de chamada da Level2Activity");
				//Intent i = new Intent(getActivity(), TestActivity.class);
				Intent i = new Intent(getActivity().getApplicationContext(), Level2Activity.class);
				Log.i("LEVEL1","item_text = " + gridArray.get(position).getId());
				i.putExtra("id_group", gridArray.get(position).getId());
				startActivity(i);
			}

		});
        
        return fragmento;
    }

}

