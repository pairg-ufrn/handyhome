package pairg.handyhome;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import pairg.handyhome.adapter.GridViewLevel2Adapter;
import android.app.ActionBar;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader.TileMode;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;


public class ProfileActivity extends Activity {

	private ActionBar actionBar;
	
	//VARIAVEIS NavDrawer 2
		String[] mOptions ;
		private DrawerLayout mDrawerLayout;
		private ListView mDrawerList;
		private ActionBarDrawerToggle mDrawerToggle;
		private LinearLayout mDrawer ;
		private List<HashMap<String,String>> mList ;
		private SimpleAdapter mAdapter;
		final private String NAME = "option_name";
		final private String ICON = "option_icon";
		int[] mFlags = new int[]{
				 R.drawable.ic_drawer_profile,
				 R.drawable.ic_drawer_home,
				 R.drawable.ic_drawer_scenes,
				 R.drawable.ic_drawer_search,
				 R.drawable.ic_drawer_about,
				 R.drawable.ic_drawer_settings,
				 R.drawable.ic_drawer_exit
		};
		final private int TAM = mFlags.length;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_withfragment);
		Log.i("PROFILE","ESTOU ENTRANDO NO ONCREATE DO PROFILE!!! \\o/");
		
		// Initialization
		actionBar = getActionBar();
		actionBar.setDisplayShowTitleEnabled(false);

		
		//Navigation Drawer 2 Init
				Log.i("PROFILE","Iniciou Navigation Drawer");
				try{
					// Pede o JSONObject 
					Log.i("PROFILE","a1");
					JSONObject myUserJSON = HTTPRequest.getUserBasicInfo();
					Log.i("PROFILE","a2");
					ArrayList<String> userinfo = JSONInterpreter.mapJSONtoUserBasicInfo(myUserJSON);
					Log.i("PROFILE","a3");
					TextView username = (TextView) findViewById(R.id.usernamedrawer4);
					Log.i("PROFILE","a4");
					username.setText(userinfo.get(0));
					Log.i("PROFILE","a5");
					ImageView userimage = (ImageView) findViewById(R.id.userimagedrawer4);
					Log.i("PROFILE","a6");
					//userimage.setImageBitmap(createRoundImage(JSONInterpreter.getImageFromName(userinfo.get(1))));
					userimage.setImageBitmap(JSONInterpreter.getImageFromName(userinfo.get(1)));
					Log.i("PROFILE","a7");
				} catch(JSONException e){
					Log.i("PROFILE","Exception!!!");
					e.printStackTrace();
				}
				
				Log.i("PROFILE","P1");
				mOptions = getResources().getStringArray(R.array.drawer_options);
				mDrawerList = (ListView) findViewById(R.id.drawer_list4);
				mDrawer = (LinearLayout) findViewById(R.id.drawer_linearlayout4);
				Log.i("PROFILE","P2");
				mList = new ArrayList<HashMap<String,String>>();
				for(int i=0;i<TAM;i++){
					HashMap<String, String> hm = new HashMap<String,String>();
					hm.put(NAME, mOptions[i]);
					hm.put(ICON, Integer.toString(mFlags[i]) );
					mList.add(hm);
				}
				Log.i("PROFILE","P3");
				/// Keys used in Hashmap
				String[] from = { ICON,NAME };
				//Ids of views in item_layout
				int[] to = { R.id.drawer_option_icon , R.id.drawer_option_name };
				Log.i("PROFILE","P4");
				// Instantiating an adapter to store each items
				mAdapter = new SimpleAdapter(this, mList, R.layout.draweritem, from, to);
				// Getting reference to DrawerLayout
				mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer4);
				// Creating a ToggleButton for NavigationDrawer with drawer event listener
				Log.i("PROFILE","P5");
				mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer , R.string.drawer_open,R.string.drawer_close){
				 // Called when drawer is closed 
				 public void onDrawerClosed(View view) {
					 //highlightSelectedCountry();
					 invalidateOptionsMenu();
				 }
				// Called when a drawer is opened 
				 public void onDrawerOpened(View drawerView) {
					 //getSupportActionBar().setTitle("Select a Country");
					 invalidateOptionsMenu();
				 }
				 };
				 Log.i("PROFILE","P6");
				 // Setting event listener for the drawer
				 mDrawerLayout.setDrawerListener(mDrawerToggle);
				 // ItemClick event handler for the drawer items
				 Log.i("PROFILE","P7");
				 mDrawerList.setOnItemClickListener(new OnItemClickListener() {
					@Override
					 public void onItemClick(AdapterView<?> arg0, View arg1, int drawerposition,
					 long arg3) {
					 mDrawerList.setItemChecked(drawerposition, false);
					 Log.i("PROFILE","P8");
					 // Closing the drawer
					 mDrawerLayout.closeDrawer(mDrawer);
					// Intent - Calling the correspondent option Activity
		             Log.i("PROFILE","Vou configurar o intent de chamada da Activity da opcao");
		             	 Intent i;
			             switch (drawerposition) {
			             case 0:  //PERFIL, NADA
						  		  break;
			             case 1:  //principal
			            	 	  i = new Intent(getApplicationContext(), MainActivity.class);
			            	 	  i.putExtra("position_drawer", drawerposition);
			            	 	  i.putExtra("option_name", mOptions[drawerposition]);
			            	 	  startActivity(i);
			            	 	  break;
			             case 2:  //cenarios (atualmente chamando tutorial)
			            	 	  i = new Intent(getApplicationContext(), ScenesActivity.class);
			            	 	  i.putExtra("position_drawer", drawerposition);
			            	 	  i.putExtra("option_name", mOptions[drawerposition]);
			            	 	  startActivity(i);
			                      break;
			             case 3:  //search
			            	 	  Toast.makeText(getBaseContext(),R.string.search_disable,Toast.LENGTH_LONG).show();
		            	 	  	  break;
			             case 4:  //sobre
			            	 	  i = new Intent(getApplicationContext(), AboutActivity.class);
			            	 	  i.putExtra("position_drawer", drawerposition);
			            	 	  i.putExtra("option_name", mOptions[drawerposition]);
			            	 	  startActivity(i);
			            	 	  break;
			             case 5:  //configuracoes
			            	 	  i = new Intent(getApplicationContext(), QuickPrefsActivity.class);
			            	 	  i.putExtra("position_drawer", drawerposition);
			            	 	  i.putExtra("option_name", mOptions[drawerposition]);
			            	 	  startActivity(i);
			                      break;
			             case 6:  //desconectar
			            	 	  HTTPRequest.logoff(); //para desconectar com o servidor
			            	 	  i = new Intent(getApplicationContext(), LoginActivity.class);
			            	 	  i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			            	 	  startActivity(i);
			            	 	  break;
			             default: i = new Intent(getApplicationContext(), TutorialActivity.class);
			 					  i.putExtra("position_drawer", drawerposition);
			 					  i.putExtra("option_name", mOptions[drawerposition]);
			 					  startActivity(i);
			                      break;
			             } 
		             
			             //fim do switch
						
					 }
				 });
				 Log.i("PROFILE","P9");
				// Enabling Home button
			    actionBar.setHomeButtonEnabled(true);
			    // Enabling Up Navigation
			    actionBar.setDisplayHomeAsUpEnabled(true);
				// Setting the adapter to the listView
				mDrawerList.setAdapter(mAdapter);
				
				//Colocar um Listener na linha de identificacao do usuario para nao fazer nada
				LinearLayout userInfoDrawerRow = (LinearLayout) findViewById(R.id.linearlayout_profile4);
				userInfoDrawerRow.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						Log.i("PROFILE","P10");
						// TODO Auto-generated method stub
					}
				});
				//Navigation Drawer 2 End 
		
				
				//AQUI O CODIGO PARA O FRAGMENTOs
				//Setando o Fragmento ABOUT
				ProfileFragment myFragment = new ProfileFragment();
				 
				//Se quiser pegar e enviar o extra da outra Activity
				Bundle extras = getIntent().getExtras();
				int extraposition = extras.getInt("position_drawer");
				String extraname = extras.getString("option_name");
				//Se quiser enviar para o Fragmento
                Bundle data = new Bundle();
                data.putInt("position_drawer", extraposition);
                data.putString("option_name", extraname);
                myFragment.setArguments(data);
                
                //Colocando Fragmento
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction ft = fragmentManager.beginTransaction();
                ft.replace(R.id.content_frame, myFragment);
                ft.commit();
		
	}
	
	
	
	
	


	
	
	
	@Override
	// Add items to the ActionBar if it is presented
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_actions, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	//methods
	@Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        Log.i("PROFILE","P12");
        mDrawerToggle.syncState();
    }
	
	
	//More Methods related to Navigation Drawer
		// Handling the touch event of app icon
	    @Override
	    public boolean onOptionsItemSelected(MenuItem item) { 
	        if (mDrawerToggle.onOptionsItemSelected(item)) {
	            return true;
	        }
	        // Handle presses on the action bar items
	        switch (item.getItemId()) {
	            case R.id.action_favorite:
	            	//Toast.makeText(getBaseContext(),"Cenas Favoritas!!! ",Toast.LENGTH_LONG).show();
	            	Intent i = new Intent(getApplicationContext(), FavoriteScenesActivity.class);
	 	 	  		startActivity(i);
	            	return true;
	            case R.id.action_refresh:
	            	Toast.makeText(getBaseContext(),R.string.refresh_message,Toast.LENGTH_SHORT).show();
	                return true;
	            case R.id.action_voice:
	            	Toast.makeText(getBaseContext(),R.string.voiceinteraction_disable,Toast.LENGTH_LONG).show();
	                return true;
	            default:
	                return super.onOptionsItemSelected(item);
	        }
	    }
    
 // Related to Navigation Drawer 2
    private Bitmap createRoundImage(Bitmap img){
		Bitmap circleBitmap = Bitmap.createBitmap(img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);
	    BitmapShader shader = new BitmapShader(img, TileMode.CLAMP, TileMode.CLAMP);
	    Paint paint = new Paint();
	    paint.setAntiAlias(true);
	    paint.setShader(shader);
	    Log.i("PROFILE","P14");
	    Canvas c = new Canvas(circleBitmap);
	    c.drawCircle(img.getWidth() / 2, img.getHeight() / 2, img.getWidth() / 2, paint);
	    return circleBitmap;
	}

}

